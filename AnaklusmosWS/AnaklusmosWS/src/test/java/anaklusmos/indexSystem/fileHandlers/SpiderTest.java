package anaklusmos.indexSystem.fileHandlers;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SpiderTest extends TestCase {

	private Spider spider;
	private String path = this.getClass().getClassLoader()
			.getResource("TextTestFile.txt").getPath();

	@Before
	protected void setUp() throws Exception {
		super.setUp();
		spider = new Spider();
		setPath();
	}

	@After
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	private void setPath() {
		path = path.substring(0, path.lastIndexOf("/"));
	}

	private String trimFileName(String fileFullPath) {
		return fileFullPath.substring(fileFullPath.lastIndexOf("/") + 1,
				fileFullPath.length());
	}

	private List<String> trimListNames(Collection<String> listFullPaths) {
		List<String> list = new LinkedList<String>();
		Iterator<String> it = listFullPaths.iterator();
		while (it.hasNext()) {
			list.add(trimFileName(it.next()));
		}
		return list;
	}

	@Test
	public void testListContentRecursive() {
		Collection<String> listRecursive = spider.listContentRecursive(path);
		assertTrue((trimListNames(listRecursive)).containsAll(getResultList()));
	}

	private List<String> getResultList() {
		List<String> list = new LinkedList<String>();
		list.add("PdfTestFile.pdf");
		list.add("TextTestFile.txt");
		list.add("ClassicExcelTestFile.xls");
		list.add("ClassicPowerpointTestFile.ppt");
		list.add("ClassicWordTestFile.doc");
		list.add("NewExcelTestFile.xlsx");
		list.add("NewPowerpointTest.pptx");
		list.add("NewWordTestFile.docx");
		list.add("RtfTestFile.rtf");
		list.add("TestFileInside.txt");
		return list;
	}
}
