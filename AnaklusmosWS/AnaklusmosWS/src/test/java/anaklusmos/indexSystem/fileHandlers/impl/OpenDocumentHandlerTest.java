package anaklusmos.indexSystem.fileHandlers.impl;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import anaklusmos.indexSystem.fileHandlers.IFileHandler;

public class OpenDocumentHandlerTest extends TestCase {

	private IFileHandler handler;

	private String pathPresentation = this.getClass().getClassLoader()
			.getResource("OOPresentation.odp").getPath();
	private String pathSpreadSheet = this.getClass().getClassLoader()
			.getResource("OOSpreadsheet.ods").getPath();
	private String pathTextDoc = this.getClass().getClassLoader()
			.getResource("OOTextDoc.odt").getPath();
	private String invalidPath = "thisIsAnInvalidPath";

	@Before
	protected void setUp() throws Exception {
		super.setUp();
		handler = new OpenDocumentHandler();
	}

	@Test
	public void testGetFileContent_validOdt() {
		boolean testOdt = true;
		try {
			testOdt = handler.getFileContent(pathTextDoc).contains(
					"This is an openoffice text document file");
		} catch (IOException e) {
			testOdt = false;
		}
		assertTrue(testOdt);
	}

	@Test
	public void testGetFileContent_validOdp() {
		boolean testOdp = true;
		try {
			testOdp = handler.getFileContent(pathPresentation).contains(
					"This is an openofffice presentation file");
		} catch (IOException e) {
			testOdp = false;
		}
		assertTrue(testOdp);
	}

	@Test
	public void testGetFileContent_validOds() {
		boolean testOds = false;
		try {
			testOds = handler.getFileContent(pathSpreadSheet).contains(
					"This is an openoffice spreadsheet file");
		} catch (IOException e) {
			testOds = true;
		}
		assertTrue(testOds);
	}

	@Test
	public void testGetFileContent_invalidFilePath() {
		boolean testDoc = false;
		try {
			handler.getFileContent(invalidPath);
		} catch (IOException e) {
			testDoc = true;
		}
		assertTrue(testDoc);
	}

	@Test
	public void testIsValidFormat() {
		assertTrue(handler.isValidFormat("file.odp")
				&& handler.isValidFormat("file.ods")
				&& handler.isValidFormat("file.odt")
				&& !handler.isValidFormat("file.pdf"));
	}

}
