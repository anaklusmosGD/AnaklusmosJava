package anaklusmos.indexSystem.fileHandlers.impl;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import anaklusmos.indexSystem.fileHandlers.IFileHandler;

public class TxtHandlerTest extends TestCase {

	private String path = this.getClass().getClassLoader()
			.getResource("TextTestFile.txt").getPath();
	private String invalidPath = "thisIsAnInvalidPath";
	private IFileHandler handler;

	@Before
	protected void setUp() throws Exception {
		handler = new TxtHandler();
		super.setUp();
	}

	@Test
	public void testGetFileContent_validFilePath() {
		String text = "no joy :_(";
		try {
			text = handler.getFileContent(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertEquals(text, "This is a text file.\n");
	}

	@Test
	public void testGetFileContent_invalidFilePath() {
		boolean errorHappened = false;
		try {
			handler.getFileContent(invalidPath);
		} catch (IOException e) {
			errorHappened = true;
		}
		Assert.assertTrue(errorHappened);
	}

	@Test
	public void testIsValidFormat() {
		assertTrue(handler.isValidFormat("file.txt")
				&& !handler.isValidFormat("file.doc")
				&& !handler.isValidFormat("file.ppt")
				&& !handler.isValidFormat("file.xls")
				&& !handler.isValidFormat("file.docx")
				&& !handler.isValidFormat("file.pptx")
				&& !handler.isValidFormat("file.xlsx")
				&& !handler.isValidFormat("file.odp")
				&& !handler.isValidFormat("file.ods")
				&& !handler.isValidFormat("file.odt")
				&& !handler.isValidFormat("file.pdf")
				&& !handler.isValidFormat("file.rtf"));
	}

}
