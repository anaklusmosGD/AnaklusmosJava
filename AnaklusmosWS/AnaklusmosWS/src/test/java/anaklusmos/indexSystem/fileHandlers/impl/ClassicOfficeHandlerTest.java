package anaklusmos.indexSystem.fileHandlers.impl;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import anaklusmos.indexSystem.fileHandlers.IFileHandler;

public class ClassicOfficeHandlerTest extends TestCase {

	private IFileHandler handler;

	private String pathExcel = this.getClass().getClassLoader()
			.getResource("ClassicExcelTestFile.xls").getPath();
	private String pathPowerpoint = this.getClass().getClassLoader()
			.getResource("ClassicPowerpointTestFile.ppt").getPath();
	private String pathWord = this.getClass().getClassLoader()
			.getResource("ClassicWordTestFile.doc").getPath();
	private String invalidPath = "thisIsAnInvalidPath";

	@Before
	protected void setUp() throws Exception {
		handler = new ClassicOfficeHandler();
		super.setUp();
	}

	@Test
	public void testGetFileContent_validWord() {
		boolean testDoc = true;
		try {
			testDoc = handler.getFileContent(pathWord).contains(
					"This is a classic word file");
		} catch (IOException e) {
			testDoc = false;
		}
		assertTrue(testDoc);
	}

	@Test
	public void testGetFileContent_validPpt() {
		boolean testPpt = true;
		try {
			testPpt = handler.getFileContent(pathPowerpoint).contains(
					"This is a classic powerpoint file");
		} catch (IOException e) {
			testPpt = false;
		}
		assertTrue(testPpt);
	}

	@Test
	public void testGetFileContent_validXls() {
		boolean testXls = true;
		try {
			testXls = handler.getFileContent(pathExcel).contains(
					"This is a classic excel file");
		} catch (IOException e) {
			testXls = false;
		}
		assertTrue(testXls);
	}

	@Test
	public void testGetFileContent_invalidFilePath() {
		boolean testDoc = false;
		try {
			handler.getFileContent(invalidPath);
		} catch (IOException e) {
			testDoc = true;
		}
		assertTrue(testDoc);
	}

	@Test
	public void testIsValidFormat() {
		assertTrue(handler.isValidFormat("file.doc")
				&& handler.isValidFormat("file.ppt")
				&& handler.isValidFormat("file.xls")
				&& !handler.isValidFormat("file.docx")
				&& !handler.isValidFormat("file.pptx")
				&& !handler.isValidFormat("file.xlsx")
				&& !handler.isValidFormat("file.odp")
				&& !handler.isValidFormat("file.ods")
				&& !handler.isValidFormat("file.odt")
				&& !handler.isValidFormat("file.pdf")
				&& !handler.isValidFormat("file.rtf")
				&& !handler.isValidFormat("file.txt"));
	}

}
