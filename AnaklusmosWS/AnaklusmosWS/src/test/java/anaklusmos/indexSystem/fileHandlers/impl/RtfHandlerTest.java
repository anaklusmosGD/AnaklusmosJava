package anaklusmos.indexSystem.fileHandlers.impl;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import anaklusmos.indexSystem.fileHandlers.IFileHandler;

public class RtfHandlerTest extends TestCase {

	private String path = this.getClass().getClassLoader()
			.getResource("RtfTestFile.rtf").getPath();
	private String invalidPath = "thisIsAnInvalidPath";
	private IFileHandler handler;

	@Before
	protected void setUp() throws Exception {
		handler = new RtfHandler();
		super.setUp();
	}

	@Test
	public void testGetFileContent_validFilePath() {
		String content = "";
		try {
			content = handler.getFileContent(path);
		} catch (IOException e) {
			fail("Couldn't access to the file " + path);
		}
		assertTrue(content.contains("This is a rtf file"));
	}
	
	@Test
	public void testGetFileContent_invalidFilePath() {
		boolean errorHappened = false;
		try {
			handler.getFileContent(invalidPath);
		} catch (IOException e) {
			errorHappened = true;
		}
		Assert.assertTrue(errorHappened);
	}

	@Test
	public void testIsValidFormat() {
		assertTrue(handler.isValidFormat("file.rtf")
				&& !handler.isValidFormat("file.doc")
				&& !handler.isValidFormat("file.ppt")
				&& !handler.isValidFormat("file.xls")
				&& !handler.isValidFormat("file.docx")
				&& !handler.isValidFormat("file.pptx")
				&& !handler.isValidFormat("file.xlsx")
				&& !handler.isValidFormat("file.odp")
				&& !handler.isValidFormat("file.ods")
				&& !handler.isValidFormat("file.odt")
				&& !handler.isValidFormat("file.pdf")
				&& !handler.isValidFormat("file.txt"));
	}

}
