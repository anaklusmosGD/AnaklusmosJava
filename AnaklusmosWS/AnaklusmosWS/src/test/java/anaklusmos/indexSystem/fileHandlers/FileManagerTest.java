package anaklusmos.indexSystem.fileHandlers;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FileManagerTest extends TestCase {

	private FileManager fileManager;
	private String pathExcel = this.getClass().getClassLoader()
			.getResource("ClassicExcelTestFile.xls").getPath();
	private String pathPowerpoint = this.getClass().getClassLoader()
			.getResource("ClassicPowerpointTestFile.ppt").getPath();
	private String pathWord = this.getClass().getClassLoader()
			.getResource("ClassicWordTestFile.doc").getPath();
	private String pathExcelx = this.getClass().getClassLoader()
			.getResource("NewExcelTestFile.xlsx").getPath();
	private String pathPowerpointx = this.getClass().getClassLoader()
			.getResource("NewPowerpointTest.pptx").getPath();
	private String pathWordx = this.getClass().getClassLoader()
			.getResource("NewWordTestFile.docx").getPath();
	private String pathPresentation = this.getClass().getClassLoader()
			.getResource("OOPresentation.odp").getPath();
	private String pathSpreadSheet = this.getClass().getClassLoader()
			.getResource("OOSpreadsheet.ods").getPath();
	private String pathTextDoc = this.getClass().getClassLoader()
			.getResource("OOTextDoc.odt").getPath();
	private String pathPdf = this.getClass().getClassLoader()
			.getResource("PdfTestFile.pdf").getPath();
	private String pathRtf = this.getClass().getClassLoader()
			.getResource("RtfTestFile.rtf").getPath();
	private String pathTxt = this.getClass().getClassLoader()
			.getResource("TextTestFile.txt").getPath();
	private String invalidPath = "thisIsAnInvalidPath";

	@Before
	protected void setUp() throws Exception {
		fileManager = new FileManager();
		super.setUp();
	}

	@Test
	public void testGetFileContent_validDoc() {
		boolean testDoc = true;
		try {
			testDoc = fileManager.getFileContent(pathWord).contains(
					"This is a classic word file");
		} catch (IOException e) {
			testDoc = false;
		}
		assertTrue(testDoc);
	}

	@Test
	public void testGetFileContent_validPpt() {
		boolean testPpt = true;
		try {
			testPpt = fileManager.getFileContent(pathPowerpoint).contains(
					"This is a classic powerpoint file");

		} catch (IOException e) {
			testPpt = false;
		}
		assertTrue(testPpt);
	}

	@Test
	public void testGetFileContent_validXls() {
		boolean testXls = true;
		try {
			testXls = fileManager.getFileContent(pathExcel).contains(
					"This is a classic excel file");

		} catch (IOException e) {
			testXls = false;
		}
		assertTrue(testXls);
	}

	@Test
	public void testGetFileContent_validDocX() {
		boolean testDocx = true;
		try {
			testDocx = fileManager.getFileContent(pathWordx).contains(
					"This is a new word file");
		} catch (IOException e) {
			testDocx = false;
		}
		assertTrue(testDocx);
	}

	@Test
	public void testGetFileContent_validPptx() {
		boolean testPptx = true;
		try {
			testPptx = fileManager.getFileContent(pathPowerpointx).contains(
					"This is new powerpoint file");

		} catch (IOException e) {
			testPptx = false;
		}
		assertTrue(testPptx);
	}

	@Test
	public void testGetFileContent_validXlsx() {
		boolean testXlsx = true;
		try {
			testXlsx = fileManager.getFileContent(pathExcelx).contains(
					"This is a new excel file");
		} catch (IOException e) {
			testXlsx = false;
		}
		assertTrue(testXlsx);
	}

	@Test
	public void testGetFileContent_validOdp() {
		boolean testOdp = true;
		try {
			testOdp = fileManager.getFileContent(pathPresentation).contains(
					"This is an openofffice presentation file");
		} catch (IOException e) {
			testOdp = false;
		}
		assertTrue(testOdp);
	}

	@Test
	public void testGetFileContent_validOds() {
		boolean testOds = true;
		try {
			testOds = fileManager.getFileContent(pathSpreadSheet).contains(
					"This is an openoffice spreadsheet file");
		} catch (IOException e) {
			testOds = false;
		}
		assertTrue(testOds);
	}

	@Test
	public void testGetFileContent_validOdt() {
		boolean testOdt = true;
		try {
			testOdt = fileManager.getFileContent(pathTextDoc).contains(
					"This is an openoffice text document file");
		} catch (IOException e) {
			System.err
					.println("There was an error accessing to the test document files.");
		}
		assertTrue(testOdt);
	}

	@Test
	public void testGetFileContent_validPdf() {
		boolean testPdf = true;
		try {
			testPdf = fileManager.getFileContent(pathPdf).contains(
					"This is a pdf test file");
		} catch (IOException e) {
			testPdf = false;
		}
		assertTrue(testPdf);
	}

	@Test
	public void testGetFileContent_validRtf() {
		boolean testRtf = true;
		try {
			testRtf = fileManager.getFileContent(pathRtf).contains(
					"This is a rtf file");
		} catch (IOException e) {
			testRtf = false;
		}
		assertTrue(testRtf);
	}

	@Test
	public void testGetFileContent() {
		boolean testTxt = true;
		try {
			testTxt = fileManager.getFileContent(pathTxt).contains(
					"This is a text file.");
		} catch (IOException e) {
			testTxt = false;
		}
		assertTrue(testTxt);
	}

	@Test
	public void testGetFileContent_invalidFilePath() {
		boolean errorHappened = false;
		try {
			fileManager.getFileContent(invalidPath);
		} catch (IOException e) {
			errorHappened = true;
		}
		Assert.assertTrue(errorHappened);
	}

	@Test
	public void testIsValidFormat() {
		assertTrue(fileManager.isValidFormat("file.xls")
				&& fileManager.isValidFormat("file.ppt")
				&& fileManager.isValidFormat("file.doc")
				&& fileManager.isValidFormat("file.xlsx")
				&& fileManager.isValidFormat("file.pptx")
				&& fileManager.isValidFormat("file.docx")
				&& fileManager.isValidFormat("file.odp")
				&& fileManager.isValidFormat("file.ods")
				&& fileManager.isValidFormat("file.odt")
				&& fileManager.isValidFormat("file.pdf")
				&& fileManager.isValidFormat("file.rtf")
				&& fileManager.isValidFormat("file.txt"));
	}

}