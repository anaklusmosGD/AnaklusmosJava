package anaklusmos.indexSystem.fileHandlers.impl;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import anaklusmos.indexSystem.fileHandlers.IFileHandler;

public class NewOfficeHandlerTest extends TestCase {

	private IFileHandler handler;

	private String pathExcelx = this.getClass().getClassLoader()
			.getResource("NewExcelTestFile.xlsx").getPath();
	private String pathPowerpointx = this.getClass().getClassLoader()
			.getResource("NewPowerpointTest.pptx").getPath();
	private String pathWordx = this.getClass().getClassLoader()
			.getResource("NewWordTestFile.docx").getPath();
	private String invalidPath = "thisIsAnInvalidPath";

	@Before
	protected void setUp() throws Exception {
		super.setUp();
		handler = new NewOfficeHandler();
	}

	@Test
	public void testGetFileContent_validDocx() {
		boolean testDocx = true;
		try {
			testDocx = handler.getFileContent(pathWordx).contains(
					"This is a new word file");
		} catch (IOException e) {
			testDocx = false;
		}
		assertTrue(testDocx);
	}

	@Test
	public void testGetFileContent_validPptx() {
		boolean testPptx = true;
		try {
			testPptx = handler.getFileContent(pathPowerpointx).contains(
					"This is new powerpoint file");
		} catch (IOException e) {
			testPptx = false;
		}
		assertTrue(testPptx);
	}

	@Test
	public void testGetFileContent_validXlsx() {
		boolean testXlsx = true;
		try {
			testXlsx = handler.getFileContent(pathExcelx).contains(
					"This is a new excel file");
		} catch (IOException e) {
			testXlsx = false;
		}
		assertTrue(testXlsx);
	}

	@Test
	public void testGetFileContent_invalidFilePath() {
		boolean testDoc = false;
		try {
			handler.getFileContent(invalidPath);
		} catch (IOException e) {
			testDoc = true;
		}
		assertTrue(testDoc);
	}

	@Test
	public void testIsValidFormat() {
		assertTrue(handler.isValidFormat("file.docx")
				&& handler.isValidFormat("file.pptx")
				&& handler.isValidFormat("file.xlsx")
				&& !handler.isValidFormat("file.doc")
				&& !handler.isValidFormat("file.ppt")
				&& !handler.isValidFormat("file.xls")
				&& !handler.isValidFormat("file.odp")
				&& !handler.isValidFormat("file.ods")
				&& !handler.isValidFormat("file.odt")
				&& !handler.isValidFormat("file.pdf")
				&& !handler.isValidFormat("file.rtf")
				&& !handler.isValidFormat("file.txt"));
	}

}