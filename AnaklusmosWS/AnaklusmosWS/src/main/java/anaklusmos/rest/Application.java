package anaklusmos.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Launches the web application.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 1.0
 * 
 * No parameters:
 * http://localhost:9000/greeting
 * Using parameters:
 * http://localhost:9000/greeting?name=User
 */
@SpringBootApplication
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
