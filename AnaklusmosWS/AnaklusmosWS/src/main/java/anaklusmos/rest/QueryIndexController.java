package anaklusmos.rest;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rest.request.RequestIndex;
import rest.request.RequestQuery;
import rest.request.RequestRemove;
import anaklusmos.indexSystem.ISearchSystem;
import anaklusmos.indexSystem.SearchSystem;

/**
 * 
 * The Query index methods are defined here.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 1.0
 *
 */
@RestController
public class QueryIndexController {

	Logger log = Logger.getLogger(QueryIndexController.class);
	private ISearchSystem searchSystem;

	@RequestMapping("/indexFiles")
	public boolean indexFiles(
			@RequestParam(value = "index", defaultValue = "null") RequestIndex request) {
		boolean success = true;
		try {
			searchSystem = new SearchSystem();
			searchSystem.indexFiles(request.getDirPath(),
					request.getDescription(), request.getTags(),
					request.getGroup());
		} catch (IOException e) {
			success = false;
			log.error(e.getMessage());
		}
		return success;
	}

	@RequestMapping("/queryContents")
	public Collection<String> queryContents(
			@RequestParam(value = "request") RequestQuery request) {
		Collection<String> results = new LinkedList<String>();
		try {
			searchSystem.queryContents(request.getStringQuery(),
					request.getMaxHits());
		} catch (ParseException | IOException e) {
			log.error(e.getMessage());
		}
		return results;
	}

	@RequestMapping("/queryDescription")
	public Collection<String> queryDescription(
			@RequestParam(value = "request") RequestQuery request) {
		Collection<String> results = new LinkedList<String>();
		try {
			searchSystem.queryDescription(request.getStringQuery(),
					request.getMaxHits());
		} catch (ParseException | IOException e) {
			log.error(e.getMessage());
		}
		return results;
	}

	@RequestMapping("/queryGroup")
	public Collection<String> queryGroup(
			@RequestParam(value = "request") RequestQuery request) {
		Collection<String> results = new LinkedList<String>();
		try {
			searchSystem.queryGroup(request.getStringQuery(),
					request.getMaxHits());
		} catch (ParseException | IOException e) {
			log.error(e.getMessage());
		}
		return results;
	}

	@RequestMapping("/queryId")
	public Collection<String> queryId(
			@RequestParam(value = "request") RequestQuery request) {
		Collection<String> results = new LinkedList<String>();
		try {
			searchSystem
					.queryId(request.getStringQuery(), request.getMaxHits());
		} catch (ParseException | IOException e) {
			log.error(e.getMessage());
		}
		return results;
	}

	@RequestMapping("/queryTags")
	public Collection<String> queryTags(
			@RequestParam(value = "request") RequestQuery request) {
		Collection<String> results = new LinkedList<String>();
		try {
			searchSystem.queryTags(request.getStringQuery(),
					request.getMaxHits());
		} catch (ParseException | IOException e) {
			log.error(e.getMessage());
		}
		return results;
	}

	@RequestMapping("/queryTitle")
	public Collection<String> queryTitle(
			@RequestParam(value = "request") RequestQuery request) {
		Collection<String> results = new LinkedList<String>();
		try {
			searchSystem.queryTitle(request.getStringQuery(),
					request.getMaxHits());
		} catch (ParseException | IOException e) {
			log.error(e.getMessage());
		}
		return results;
	}

	@RequestMapping("/addDoc")
	public boolean addDoc(
			@RequestParam(value = "title", defaultValue = "default") String title,
			@RequestParam(value = "description", defaultValue = "default") String description,
			@RequestParam(value = "tags", defaultValue = "default") String tags,
			@RequestParam(value = "filePath", defaultValue = "default") String filePath,
			@RequestParam(value = "group", defaultValue = "default") String group) {
		boolean success = true;
		try {
			searchSystem = new SearchSystem();
			searchSystem.addDoc(title, description, tags, filePath, group);
		} catch (IOException e) {
			success = false;
			log.error(e.getMessage());
		}
		return success;
	}

	@RequestMapping("/removeAllDocs")
	public boolean removeAllDocs() {
		boolean success = true;
		try {
			searchSystem = new SearchSystem();
			searchSystem.removeAllDocs();
		} catch (IOException e) {
			success = false;
			log.error(e.getMessage());
		}
		return success;
	}

	@RequestMapping("/removeDoc")
	public boolean removeDoc(
			@RequestParam(value = "request", defaultValue = "null") RequestRemove request) {
		boolean success = true;
		try {
			searchSystem = new SearchSystem();
			searchSystem.removeDoc(request.getTitle(), request.getGroup());
		} catch (IOException e) {
			success = false;
			log.error(e.getMessage());
		}
		return success;
	}

}
