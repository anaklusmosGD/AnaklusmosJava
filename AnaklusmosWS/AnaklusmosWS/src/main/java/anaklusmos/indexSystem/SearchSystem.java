package anaklusmos.indexSystem;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;

import anaklusmos.indexSystem.fileHandlers.Spider;

/**
 * Search system handler
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 1.0
 */
public class SearchSystem implements ISearchSystem {

	/**
	 * The home directory, to index the files.
	 */
	private String homeDir;
	/**
	 * The analyzer
	 */
	private Analyzer analyzer;

	/**
	 * The new index.
	 */
	private FSDirectory index;

	/**
	 * The query.
	 */
	private Query query;

	/**
	 * The search index.
	 */
	private IndexSearcher searcher;

	/**
	 * The results found.
	 */
	private ScoreDoc[] hits;
	/**
	 * The log system.
	 */
	private Logger log = Logger.getLogger(SearchSystem.class);

	/**
	 * Step 0: specifies the analyzer to get the text tokens. It will be used to
	 * index and search into files.
	 * 
	 * @throws IOException
	 *             if we can't access to the index.
	 */
	public SearchSystem() throws IOException {
		setPathHome();
		analyzer = new StandardAnalyzer(Version.LUCENE_40);
		index = FSDirectory.open(new File(homeDir));
	}

	/**
	 * Sets the home path.
	 */
	private void setPathHome() {
		homeDir = this.getClass().getClassLoader()
				.getResource("log4j.properties").getPath();
		homeDir = homeDir.substring(0, homeDir.lastIndexOf("/"));
		homeDir = homeDir + "/lucene_output_dir_index";
	}

	/**
	 * Step 1: creates the index.
	 * 
	 * @param dirPath
	 *            the path of the directory to index.
	 * @param description
	 *            some description to index the files.
	 * @param tags
	 *            some tags to index the files.
	 * @param group
	 *            the group whose it belongs.
	 * @throws IOException
	 *             if we can't access to the index.
	 * @throws LockObtainFailedException
	 *             if we can't open to the index.
	 * @throws CorruptIndexException
	 *             if the index is corrupt.
	 */
	public void indexFiles(String dirPath, String description, String tags,
			String group) throws CorruptIndexException,
			LockObtainFailedException, IOException {
		Spider spider = new Spider();
		Collection<String> list = spider.listContentRecursive(dirPath);
		Iterator<String> it = list.iterator();
		while (it.hasNext()) {
			String filePath = it.next();
			String fileName = filePath.substring(filePath.lastIndexOf("/") + 1,
					filePath.length());
			addDoc(fileName, description, tags, filePath, group);
		}
	}

	/**
	 * Step 2a: generates the content query. Executes a query.
	 * 
	 * @param stringQuery
	 *            the query to use.
	 * @param maxHits
	 *            maximum number of results to get.
	 * @throws ParseException
	 *             if the index is corrupted.
	 * @throws CorruptIndexException
	 *             if the index is corrupted.
	 * @throws IOException
	 *             if we can't access to the index.
	 * @return the results of the query.
	 */
	public Collection<String> queryContents(String stringQuery, int maxHits)
			throws ParseException, CorruptIndexException, IOException {
		query = new QueryParser(Version.LUCENE_40, "content", analyzer)
				.parse(stringQuery);
		search(maxHits);
		return showResults();
	}

	/**
	 * Step 2b: generates the description query. Executes a query.
	 * 
	 * @param queryString
	 *            the String we will search for.
	 * @param maxHits
	 *            maximum number of results to get.v
	 * @throws ParseException
	 *             if the index is corrupted.
	 * @throws CorruptIndexException
	 *             if the index is corrupted.
	 * @throws IOException
	 *             if we can't access to the index.
	 * @return the results of the query.
	 * 
	 */
	public Collection<String> queryDescription(String queryString, int maxHits)
			throws CorruptIndexException, IOException, ParseException,
			CorruptIndexException, IOException {
		query = new QueryParser(Version.LUCENE_40, "description", analyzer)
				.parse(queryString);
		search(maxHits);
		return showResults();
	}

	/**
	 * Step 2c: generates the group query. Executes a query.
	 * 
	 * @param queryString
	 *            the String we will search for.
	 * @param maxHits
	 *            maximum number of results to get.v
	 * @throws ParseException
	 *             if the index is corrupted.
	 * @throws CorruptIndexException
	 *             if the index is corrupted.
	 * @throws IOException
	 *             if we can't access to the index.
	 * @return the results of the query.
	 * 
	 */
	public Collection<String> queryGroup(String queryString, int maxHits)
			throws ParseException, CorruptIndexException, IOException {
		query = new QueryParser(Version.LUCENE_40, "group", analyzer)
				.parse(queryString);
		search(maxHits);
		return showResults();
	}

	/**
	 * Step 2d: generates the id query. Executes a query.
	 * 
	 * @param queryString
	 *            the String we will search for.
	 * @param maxHits
	 *            maximum number of results to get.
	 * @throws ParseException
	 *             if the index is corrupted.
	 * @throws CorruptIndexException
	 *             if the index is corrupted.
	 * @throws IOException
	 *             if we can't access to the index.
	 * @return the results of the query.
	 * 
	 */
	public Collection<String> queryId(String queryString, int maxHits)
			throws ParseException, CorruptIndexException, IOException {
		query = new QueryParser(Version.LUCENE_40, "id", analyzer)
				.parse(queryString);
		search(maxHits);
		return showResults();
	}

	/**
	 * Step 2e: generates the tags query. Executes a query.
	 * 
	 * @param queryString
	 *            the String we will search for.
	 * @param maxHits
	 *            maximum number of results to get.
	 * @throws ParseException
	 *             if the index is corrupted.
	 * @throws CorruptIndexException
	 *             if the index is corrupted.
	 * @throws IOException
	 *             if we can't access to the index.
	 * @return the results of the query.
	 * 
	 */
	public Collection<String> queryTags(String queryString, int maxHits)
			throws ParseException, CorruptIndexException, IOException {
		query = new QueryParser(Version.LUCENE_40, "tags", analyzer)
				.parse(queryString);
		search(maxHits);
		return showResults();
	}

	/**
	 * Step 2f: generates the title query. Executes a query.
	 * 
	 * @param queryString
	 *            the String we will search for.
	 * @param maxHits
	 *            maximum number of results to get.
	 * @throws ParseException
	 *             if the index is corrupted.
	 * @throws CorruptIndexException
	 *             if the index is corrupted.
	 * @throws IOException
	 *             if we can't access to the index.
	 * @return the results of the query.
	 * 
	 */
	public Collection<String> queryTitle(String queryString, int maxHits)
			throws ParseException, CorruptIndexException, IOException {
		query = new QueryParser(Version.LUCENE_40, "title", analyzer)
				.parse(queryString);
		search(maxHits);
		return showResults();
	}

	/**
	 * Step 3: executes the search in the indexed info.
	 * 
	 * @param maxHits
	 *            maximum number of results to get.
	 * @throws CorruptIndexException
	 *             if the index is corrupted.
	 * @throws IOException
	 *             if we can't access to the index.
	 * @return the results of the query.
	 */
	private Collection<String> search(int maxHits)
			throws CorruptIndexException, IOException {
		int hitsPerPage = maxHits;
		@SuppressWarnings("deprecation")
		IndexReader reader = IndexReader.open(index);
		searcher = new IndexSearcher(reader);
		TopScoreDocCollector collector = TopScoreDocCollector.create(
				hitsPerPage, true);
		searcher.search(query, collector);
		hits = collector.topDocs().scoreDocs;
		return showResults();
	}

	/**
	 * Shows the results.
	 * 
	 * @throws CorruptIndexException
	 *             if the index is corrupted.
	 * @throws IOException
	 *             if we can't access to the index.
	 */
	private Collection<String> showResults() throws CorruptIndexException,
			IOException {
		Collection<String> results = new TreeSet<String>();
		for (int i = 0; i < hits.length; ++i) {
			int docId = hits[i].doc;
			Document d = searcher.doc(docId);
			results.add(d.get("title"));
		}
		return results;
	}

	/**
	 * Adds or updates a new element to the index.
	 * 
	 * @param title
	 *            the document title.
	 * @param description
	 *            the document description.
	 * @param tags
	 *            the document tags.
	 * @param filePath
	 *            the path of the file, where it's stored.
	 * @param group
	 *            the user group whose it belongs.
	 * @throws IOException
	 *             if there's an error in an input/output process.
	 */
	public void addDoc(String title, String description, String tags,
			String filePath, String group) throws IOException {
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40,
				analyzer);
		IndexWriter writer = new IndexWriter(index, config);
		DocumentToIndex myDocument = new DocumentToIndex(title, description,
				tags, filePath, group);
		Document doc = new Document();
		doc.add(new TextField("title", myDocument.getTitle(), Field.Store.YES));
		doc.add(new TextField("description", myDocument.getDescription(),
				Field.Store.YES));
		doc.add(new TextField("tags", myDocument.getTags(), Field.Store.YES));
		doc.add(new TextField("content", myDocument.getContent(),
				Field.Store.YES));
		doc.add(new TextField("group", myDocument.getGroup(), Field.Store.YES));
		doc.add(new TextField("id", myDocument.getId(), Field.Store.YES));
		try {
			Term idTerm = new Term("id", myDocument.getId());
			try {
				if (queryId(myDocument.getId(), 1).size() > 0) {
					writer.updateDocument(idTerm, doc);
				} else {
					writer.addDocument(doc);
				}
			} catch (ParseException e) {
				log.warn("Error trying to add or update the file "
						+ myDocument.getTitle() + " for the group "
						+ myDocument.getGroup()
						+ " because we couldn't parse the files");
			}
		} catch (IOException e) {
			log.warn("Error trying to add or update the file "
					+ myDocument.getTitle() + " for the group "
					+ myDocument.getGroup()
					+ " because we couldn't access to the files");
		}
		writer.close();
	}

	/**
	 * Removes every element form the index.
	 * 
	 * @throws IOException
	 *             if there's an error in an input/output process.
	 * @throws CorruptIndexException
	 *             if the index is corrupt.
	 * @throws LockObtainFailedException
	 *             if we couldn't access to the file because it was protected.
	 */
	public void removeAllDocs() throws CorruptIndexException,
			LockObtainFailedException, IOException {
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40,
				analyzer);
		IndexWriter w = new IndexWriter(index, config);
		w.deleteAll();
		w.close();
	}

	/**
	 * Removes an element to the index.
	 * 
	 * @param title
	 *            the document title.
	 * @param group
	 *            the group who owns the document.
	 * @throws IOException
	 *             if there's an error in an input/output process.
	 */
	public void removeDoc(String title, String group) throws IOException {
		try {
			Query query = new QueryParser(Version.LUCENE_40, "id", analyzer)
					.parse("title:" + title + " AND group:" + group);
			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40,
					analyzer);
			IndexWriter w = new IndexWriter(index, config);
			w.deleteDocuments(query);
			w.close();
		} catch (ParseException e) {
			System.err.println("Couldn't parse the expression.");
			e.printStackTrace();
		}
	}
}