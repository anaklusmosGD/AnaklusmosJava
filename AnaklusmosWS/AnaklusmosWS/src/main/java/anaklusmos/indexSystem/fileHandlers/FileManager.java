package anaklusmos.indexSystem.fileHandlers;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import anaklusmos.indexSystem.fileHandlers.impl.ClassicOfficeHandler;
import anaklusmos.indexSystem.fileHandlers.impl.NewOfficeHandler;
import anaklusmos.indexSystem.fileHandlers.impl.OpenDocumentHandler;
import anaklusmos.indexSystem.fileHandlers.impl.PdfHandler;
import anaklusmos.indexSystem.fileHandlers.impl.RtfHandler;
import anaklusmos.indexSystem.fileHandlers.impl.TxtHandler;

/**
 * Controls which kind of file are we inspecting, and is able to get its
 * content.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 1.0
 */
public class FileManager {

	/**
	 * Initializes a list of possible handler.
	 * 
	 * @return a list of possible handler.
	 */
	private List<IFileHandler> initialize() {
		List<IFileHandler> handlers = new LinkedList<IFileHandler>();
		handlers.add(new ClassicOfficeHandler());
		handlers.add(new NewOfficeHandler());
		handlers.add(new OpenDocumentHandler());
		handlers.add(new PdfHandler());
		handlers.add(new RtfHandler());
		handlers.add(new TxtHandler());
		return handlers;
	}

	/**
	 * Gets the correct handler to handle the file.
	 * 
	 * @param filePath
	 *            the path of the file we will work with.
	 * @return a handler if there is one which supports the extension, null if
	 *         there isn't one.
	 */
	private IFileHandler getCorrectHandler(String filePath) {
		IFileHandler handler = null;
		boolean valid = false;
		Iterator<IFileHandler> it = initialize().iterator();
		IFileHandler actualHandler = new ClassicOfficeHandler();
		while (!valid && it.hasNext()) {
			actualHandler = it.next();
			valid = valid || actualHandler.isValidFormat(filePath);
		}
		if (valid) {
			handler = actualHandler;
		}
		return handler;
	}

	/**
	 * Gets the file content if format is valid.
	 * 
	 * @param filePath
	 *            the full name of the file.
	 * @return the whole file content.
	 * @throws IOException
	 *             if we couldn't access to the file..
	 */
	public String getFileContent(String filePath) throws IOException {
		IFileHandler actualHandler = getCorrectHandler(filePath);
		String content = "";
		if (actualHandler != null) {
			content = actualHandler.getFileContent(filePath);
		} else {
			throw new IOException("Invalid file format: '" + filePath
					+ "' hasn't a valid file format.");
		}
		return content;
	}

	/**
	 * Checks if the format is valid.
	 * 
	 * @param fileName
	 *            the name of the file we will check.
	 * @return true if the format is valid, otherwise returns false.
	 */
	public boolean isValidFormat(String fileName) {
		IFileHandler actualHandler = getCorrectHandler(fileName);
		boolean valid = false;
		if (actualHandler != null) {
			valid = true;
		}
		return valid;
	}

}
