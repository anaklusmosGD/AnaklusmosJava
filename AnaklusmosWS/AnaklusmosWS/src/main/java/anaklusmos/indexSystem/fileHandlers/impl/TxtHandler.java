package anaklusmos.indexSystem.fileHandlers.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import anaklusmos.indexSystem.fileHandlers.IFileHandler;

/**
 * Class to quickly manage txt files.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 1.0
 * 
 */
public class TxtHandler implements IFileHandler {

	/**
	 * The log system.
	 */
	private static Logger log = Logger.getLogger(TxtHandler.class);

	/**
	 * Gets the file content if it belongs to docx, pptx or xlsx format.
	 * 
	 * @param filePath
	 *            the full name of the file.
	 * @return the whole file content.
	 * @throws IOException
	 *             if we couldn't access to the file..
	 */
	public String getFileContent(String filePath) throws IOException {
		String content = "";
		if (filePath.endsWith(".txt")) {
			content = readAll(filePath);
		} else {
			throw new IOException("Invalid file format: '" + filePath
					+ "' is not a txt file.");
		}
		return content;
	}

	/**
	 * Checks if the format is valid.
	 * 
	 * @param fileName
	 *            the name of the file we will check.
	 * @return true if the format is valid, otherwise returns false.
	 */
	public boolean isValidFormat(String filePath) {
		return filePath.endsWith(".txt");
	}

	/**
	 * Reads the file content line by line, saving each one of them in an item
	 * list.
	 * 
	 * @param path
	 *            the path of the file we want to read.
	 * @return the content, one line per Strings.
	 */
	private static List<String> read(String path) {
		List<String> content = new LinkedList<String>();
		File file = new File(path);
		FileReader fr = null;
		BufferedReader br = null;
		String read = "";

		// Reads the file
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			read = br.readLine();
			while (read != null) {
				content.add(read);
				read = br.readLine();
			}
		} catch (FileNotFoundException e) {
			log.error("There was an error accesing to the file.");
			log.error(e.getStackTrace());
		} catch (Exception e) {
			log.error("There was an error while reading the file.");
			log.error(e.getStackTrace());
		} finally {
			// we close the file
			try {
				if (fr != null) {
					fr.close();
				}
			} catch (Exception e) {
				log.error("We couldn't close the file.");
			}

		}
		return content;
	}

	/**
	 * Reads the file content line by line, saving all of it in single string.
	 * 
	 * @param path
	 *            the path of the file we want to read.
	 * @return the whole file content in a single String.
	 */
	private static String readAll(String path) {
		log.info("Parsing the file " + path);
		String content = "";
		List<String> listContent = read(path);
		for (int i = 0; i < listContent.size(); i++) {
			content = content + listContent.get(i) + "\n";
		}
		return content;
	}
}
