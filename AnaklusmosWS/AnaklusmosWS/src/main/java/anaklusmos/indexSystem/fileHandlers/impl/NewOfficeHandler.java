package anaklusmos.indexSystem.fileHandlers.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.xslf.XSLFSlideShow;
import org.apache.poi.xslf.extractor.XSLFPowerPointExtractor;
import org.apache.poi.xssf.extractor.XSSFExcelExtractor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.xmlbeans.XmlException;

import anaklusmos.indexSystem.fileHandlers.IFileHandler;

/**
 * Class to manage Office files: .docx, .pptx and .xlsx.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 1.0
 * 
 */
public class NewOfficeHandler implements IFileHandler {

	/**
	 * The log system.
	 */
	private static Logger log = Logger.getLogger(NewOfficeHandler.class);

	/**
	 * Gets the file content if it belongs to docx, pptx or xlsx format.
	 * 
	 * @param filePath
	 *            the full name of the file.
	 * @return the whole file content.
	 * @throws IOException
	 *             if we couldn't access to the file..
	 */
	public String getFileContent(String filePath) throws IOException {
		String content = "";
		if (filePath.endsWith(".docx")) {
			content = readDocx(filePath);
		} else {
			if (filePath.endsWith(".pptx")) {
				content = readPptx(filePath);
			} else {
				if (filePath.endsWith(".xlsx")) {
					content = readXlsx(filePath);
				} else {
					throw new IOException("Invalid file format: '" + filePath
							+ "' is not a new office file.");
				}
			}
		}
		return content;
	}

	/**
	 * Checks if the format is valid.
	 * 
	 * @param fileName
	 *            the name of the file we will check.
	 * @return true if the format is valid, otherwise returns false.
	 */
	public boolean isValidFormat(String filePath) {
		return filePath.endsWith(".docx") || filePath.endsWith(".pptx")
				|| filePath.endsWith(".xlsx");
	}

	/**
	 * Reads a new MS Word file (after Office 2007).
	 * 
	 * @param fileName
	 *            the name of the file to process.
	 * @return the file content in a String.
	 * @throws IOException
	 *             if we couldn't access the file.
	 */
	private static String readDocx(String fileName) throws IOException {
		log.info("Parsing MS word extended " + fileName);
		String data = "";
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(fileName);
			XWPFDocument docx = new XWPFDocument(fis);
			XWPFWordExtractor wordxExtractor = new XWPFWordExtractor(docx);
			data = data + wordxExtractor.getText();
			wordxExtractor.close();
			fis.close();
		} catch (FileNotFoundException e) {
			log.error("We couldn't access to the file " + fileName);
			throw new FileNotFoundException("We couldn't access to the file  "
					+ fileName);
		} catch (IOException e) {
			log.error("There was a WordX error while working with " + fileName);
			throw new IOException("There was a Word error while working with "
					+ fileName);
		}
		return data;
	}

	/**
	 * Reads a new MS PowerPoint file (after Office 2007).
	 * 
	 * @param fileName
	 *            the name of the file to process.
	 * @return the file content in a String.
	 * @throws IOException
	 *             if we couldn't access the file.
	 */
	private static String readPptx(String fileName) throws IOException {
		log.info("Parsing MS powerpoint extended " + fileName);
		String data = "";
		InputStream is = new FileInputStream(fileName);
		XSLFSlideShow pptx;
		try {
			pptx = new XSLFSlideShow(fileName);
			data = new XSLFPowerPointExtractor(
					pptx).getText();
		} catch (OpenXML4JException e) {
			log.error("There was a MS Office error while parsing " + fileName);
		} catch (XmlException e) {
			log.error("The was a XML error while parsing " + fileName);
		}
		is.close();

		return data;
	}

	/**
	 * Reads a new MS Excel file (after Office 2007).
	 * 
	 * @param fileName
	 *            the name of the file to process.
	 * @return the file content in a String.
	 * @throws IOException
	 *             if we couldn't access the file.
	 */
	private static String readXlsx(String fileName) throws IOException {
		log.info("Parsing MS excel extended " + fileName);
		String data = "";
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(fileName);
			XSSFExcelExtractor extractor = new XSSFExcelExtractor(
					new XSSFWorkbook(fis));
			data = extractor.getText();
			extractor.close();
		} catch (IOException e) {
			throw new IOException("Incorrect file type, " + fileName
					+ " isn't xlsx");
		}
		fis.close();
		return data;
	}

}
