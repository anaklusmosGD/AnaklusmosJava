package anaklusmos.indexSystem.fileHandlers;

import java.io.IOException;

/**
 * Interface for file management.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 1.0
 * 
 */
public interface IFileHandler {
	
	/**
	 * Gets the file content if it's valid.
	 * 
	 * @param filePath
	 *            the full name of the file.
	 * @return the whole file content.
	 * @throws IOException
	 *             if we couldn't access to the file..
	 */
	public abstract String getFileContent(String filePath) throws IOException;

	/**
	 * Checks if the format is valid.
	 * 
	 * @param fileName
	 *            the name of the file we will check.
	 * @return true if the format is valid, otherwise returns false.
	 */
	public abstract boolean isValidFormat(String filePath);

}
