package anaklusmos.indexSystem.fileHandlers.impl;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.w3c.dom.DOMException;

import anaklusmos.indexSystem.fileHandlers.IFileHandler;

/**
 * Class to manage Open Document files: .ods, .pdp and .odt.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 1.0
 * 
 */
public class OpenDocumentHandler implements IFileHandler {

	/**
	 * The log system.
	 */
	private static Logger log = Logger.getLogger(OpenDocumentHandler.class);

	/**
	 * Gets the file content if it belongs to odp, ods or odt format.
	 * 
	 * @param filePath
	 *            the full name of the file.
	 * @return the whole file content.
	 * @throws IOException
	 *             if we couldn't access to the file..
	 */
	public String getFileContent(String filePath) throws IOException {
		String content = "";
		if (isValidFormat(filePath)) {
			content = readOpenOffice(filePath);
		} else {
			throw new IOException("Invalid file format: '" + filePath
					+ "' is not an openoffice file.");
		}
		return content;
	}

	/**
	 * Checks if the format is valid.
	 * 
	 * @param fileName
	 *            the name of the file we will check.
	 * @return true if the format is valid, otherwise returns false.
	 */
	public boolean isValidFormat(String filePath) {
		return filePath.endsWith(".odp") || filePath.endsWith(".ods")
				|| filePath.endsWith(".odt");
	}

	/**
	 * Reads an Open OpenDocument file.
	 * 
	 * @param fileName
	 *            the name of the file to process.
	 * @return the file content in a String.
	 * @throws IOException
	 *             if we couldn't access the file.
	 */
	private static String readOpenOffice(String fileName) throws IOException {
		String data = "";
		try {
			OdfDocument document = OdfDocument.loadDocument(fileName);
			data = document.getContentRoot().getTextContent();
		} catch (IOException e) {
			log.error("There was an OpenDocument error while working with "
					+ fileName);
			throw new IOException(
					"There was an OpenDocument error while working with "
							+ fileName);
		} catch (DOMException e) {
			log.error("There was an OpenDocument error while working with DOM at "
					+ fileName);
			throw new IOException(
					"There was an OpenDocument error while working with "
							+ fileName);
		} catch (Exception e) {
			log.error("There was an OpenDocument error while working with "
					+ fileName + " at the load process");
			throw new IOException(
					"There was an OpenDocument error while working with "
							+ fileName);
		}
		return data;
	}

}
