package anaklusmos.indexSystem.fileHandlers.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.apache.poi.hslf.extractor.PowerPointExtractor;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import anaklusmos.indexSystem.fileHandlers.IFileHandler;

/**
 * Class to manage Office files: .doc, .ppt and .xls.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 1.0
 * 
 */
public class ClassicOfficeHandler implements IFileHandler {

	/**
	 * The log system.
	 */
	private static Logger log = Logger.getLogger(ClassicOfficeHandler.class);

	/**
	 * Gets the file content if it belongs to doc, ppt or xls format.
	 * 
	 * @param filePath
	 *            the full name of the file.
	 * @return the whole file content.
	 * @throws IOException
	 *             if we couldn't access to the file..
	 */
	public String getFileContent(String filePath) throws IOException {
		String content = "";
		if (filePath.endsWith(".doc")) {
			content = readDoc(filePath);
		} else {
			if (filePath.endsWith(".ppt")) {
				content = readPpt(filePath);
			} else {
				if (filePath.endsWith(".xls")) {
					content = readXls(filePath);
				} else {
					throw new IOException("Invalid file format: '" + filePath
							+ "' is not a classic office file.");
				}
			}
		}
		return content;
	}

	/**
	 * Checks if the format is valid.
	 * 
	 * @param fileName
	 *            the name of the file we will check.
	 * @return true if the format is valid, otherwise returns false.
	 */
	public boolean isValidFormat(String filePath) {
		return filePath.endsWith(".doc") || filePath.endsWith(".ppt")
				|| filePath.endsWith(".xls");
	}

	/**
	 * Reads a classic MS Word file.
	 * 
	 * @param fileName
	 *            the name of the file to process.
	 * @return the file content in a String.
	 * @throws IOException
	 *             if we couldn't access the file.
	 */
	private static String readDoc(String fileName) throws IOException {
		log.info("Parsing MS word " + fileName);
		String data = "";
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(fileName);
			HWPFDocument word = new HWPFDocument(fis);
			data = data + word.getDocumentText();
			fis.close();
		} catch (FileNotFoundException e) {
			log.error("We couldn't access to the file " + fileName);
			throw new IOException("The file " + fileName + " wasn't found.");
		} catch (IOException e) {
			log.error("There was a Word error while working with " + fileName);
			throw new IOException("There was a Word error while working with "
					+ fileName);
		}
		return data;
	}

	/**
	 * Reads a classic MS PowerPoint file.
	 * 
	 * @param fileName
	 *            the name of the file to process.
	 * @return the file content in a String.
	 * @throws IOException
	 *             if we couldn't access the file.
	 */
	private static String readPpt(String fileName) throws IOException {
		log.info("Parsing MS powerpoint " + fileName);
		String data = "";
		InputStream is = new FileInputStream(fileName);
		PowerPointExtractor extractor = new PowerPointExtractor(is);
		// gets the normal and annotations text.
		data = extractor.getText(true, true);
		data = data + extractor.getNotes();
		is.close();
		extractor.close();
		return data;
	}

	/**
	 * Reads a classic MS Excel file.
	 * 
	 * @param fileName
	 *            the name of the file to process.
	 * @return the file content in a String.
	 * @throws IOException
	 *             if we couldn't access the file.
	 */
	private static String readXls(String fileName) throws IOException {
		log.info("Parsing MS excel " + fileName);
		String data = "";
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(fileName);
			// creates an excel book.
			HSSFWorkbook workbook = new HSSFWorkbook(fis);
			int numberOfSheets = workbook.getNumberOfSheets();
			for (int i = 0; i < numberOfSheets; i++) {
				HSSFSheet sheet = workbook.getSheetAt(i);
				int numberOfRows = sheet.getPhysicalNumberOfRows();
				if (numberOfRows > 0) {
					if (workbook.getSheetName(i) != null
							&& workbook.getSheetName(i).length() != 0) {
						// appending the sheet name to content
						if (i > 0) {
							data = data + ("\n\n");
						}
						data = data + (workbook.getSheetName(i).trim());
						data = data + (":\n");
					}
					Iterator<Row> rowIt = sheet.rowIterator();
					while (rowIt.hasNext()) {
						Row row = rowIt.next();
						if (row != null) {
							boolean hasContent = false;
							Iterator<Cell> it = row.cellIterator();
							while (it.hasNext()) {
								Cell cell = it.next();
								String text = null;
								switch (cell.getCellType()) {
								case HSSFCell.CELL_TYPE_BLANK:
								case HSSFCell.CELL_TYPE_ERROR:
									// ignore all blank or error cells
									break;
								case HSSFCell.CELL_TYPE_NUMERIC:
									text = Double.toString(cell
											.getNumericCellValue());
									break;
								case HSSFCell.CELL_TYPE_BOOLEAN:
									text = Boolean.toString(cell
											.getBooleanCellValue());
									break;
								case HSSFCell.CELL_TYPE_STRING:
								default:
									text = cell.getStringCellValue();
									break;
								}
								if ((text != null) && (text.length() != 0)) {
									data = data + (text.trim());
									data = data + (' ');
									hasContent = true;
								}
							}
							if (hasContent) {
								data = data + ('\n');
							}
						}
					}
				}
			}
			workbook.close();
			fis.close();
		} catch (FileNotFoundException e) {
			log.error("We couldn't access to the file " + fileName);
			throw new FileNotFoundException("We couldn't access to the file "
					+ fileName);
		}
		return data;
	}

}
