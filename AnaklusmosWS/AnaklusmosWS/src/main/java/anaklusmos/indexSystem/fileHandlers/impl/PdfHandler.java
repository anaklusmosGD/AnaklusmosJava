package anaklusmos.indexSystem.fileHandlers.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import anaklusmos.indexSystem.fileHandlers.IFileHandler;

/**
 * Class which controls the the reading process for Pdf files. Uses PDFBox:
 * http://pdfbox.apache.org/
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 1.0
 */
public class PdfHandler implements IFileHandler {

	/**
	 * The log system.
	 */
	private static Logger log = Logger.getLogger(PdfHandler.class);

	/**
	 * Gets the file content if it belongs to docx, pptx or xlsx format.
	 * 
	 * @param filePath
	 *            the full name of the file.
	 * @return the whole file content.
	 * @throws IOException
	 *             if we couldn't access to the file..
	 */
	public String getFileContent(String filePath) throws IOException {
		String content = "";
		if (filePath.endsWith(".pdf")) {
			content = extractPdfText(filePath);
		} else {
			throw new IOException("Invalid file format: '" + filePath
					+ "' is not a pdf file.");
		}
		return content;
	}

	/**
	 * Checks if the format is valid.
	 * 
	 * @param fileName
	 *            the name of the file we will check.
	 * @return true if the format is valid, otherwise returns false.
	 */
	public boolean isValidFormat(String filePath) {
		return filePath.endsWith(".pdf");
	}

	/**
	 * Extracts the text form the pdf and puts it in a String.
	 * 
	 * @param path
	 *            the path of the file whose contents we want to get.
	 * @return a String with the pdf contents.
	 * @throws IOException
	 *             if we can't open the file, or we can't parse it.
	 */
	private static String extractPdfText(String path) throws IOException {
		log.info("Parsing pdf from " + path);
		String parsedText = "";

		File f = new File(path);
		if (!f.isFile()) {
			log.error("The file " + path + " doesn't exist.");
			return "";
		}

		if (path.endsWith(".pdf")) {
			PDFParser parser = new PDFParser(new FileInputStream(f));
			parser.parse();
			COSDocument cosDoc = parser.getDocument();
			PDFTextStripper pdfStripper = new PDFTextStripper();
			PDDocument pdDoc = new PDDocument(cosDoc);
			parsedText = pdfStripper.getText(pdDoc);
			cosDoc.close();
		} else {
			throw new IOException("Incorrect file type, " + path + " isn't pdf");
		}
		return parsedText;
	}
}
