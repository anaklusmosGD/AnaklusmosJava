package anaklusmos.indexSystem.fileHandlers.impl;

import java.io.FileInputStream;
import java.io.IOException;

import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.rtf.RTFEditorKit;

import org.apache.log4j.Logger;

import anaklusmos.indexSystem.fileHandlers.IFileHandler;

/**
 * Class to quickly manage rtf files.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 1.0
 * 
 */
public class RtfHandler implements IFileHandler {

	/**
	 * The log system.
	 */
	private static Logger log = Logger.getLogger(RtfHandler.class);

	/**
	 * Gets the file content if it belongs to docx, pptx or xlsx format.
	 * 
	 * @param filePath
	 *            the full name of the file.
	 * @return the whole file content.
	 * @throws IOException
	 *             if we couldn't access to the file..
	 */
	public String getFileContent(String filePath) throws IOException {
		String content = "";
		if (filePath.endsWith(".rtf")) {
			content = readRtf(filePath);
		} else {
			throw new IOException("Invalid file format: '" + filePath
					+ "' is not a rtf file.");
		}
		return content;
	}

	/**
	 * Checks if the format is valid.
	 * 
	 * @param fileName
	 *            the name of the file we will check.
	 * @return true if the format is valid, otherwise returns false.
	 */
	public boolean isValidFormat(String filePath) {
		return filePath.endsWith(".rtf");
	}

	/**
	 * Reads an RTF file.
	 * 
	 * @param fileName
	 *            the name of the file to process.
	 * @return the file content in a String.
	 * @throws IOException
	 *             if we couldn't access the file.
	 */
	private static String readRtf(String fileName) throws IOException {
		log.info("Parsing RTF " + fileName);
		String data = "";
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(fileName);
			DefaultStyledDocument styledDoc = new DefaultStyledDocument();
			new RTFEditorKit().read(fis, styledDoc, 0);
			data = styledDoc.getText(0, styledDoc.getLength());
		} catch (IOException e) {
			throw new IOException("Incorrect file type, " + fileName
					+ " isn't rtf");
		} catch (BadLocationException e) {
			throw new IOException("Incorrect file location, " + fileName);
		}
		fis.close();
		return data;
	}

}
