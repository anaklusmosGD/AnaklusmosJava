package anaklusmos.indexSystem;

import java.io.IOException;
import java.util.Collection;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.store.LockObtainFailedException;

/**
 * Search system handler
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 1.0
 */
public interface ISearchSystem {

	/**
	 * Step 1: creates the index.
	 * 
	 * @param dirPath
	 *            the path of the directory to index.
	 * @param description
	 *            some description to index the files.
	 * @param tags
	 *            some tags to index the files
	 * @param group
	 *            the group whose it belongs.
	 * @throws IOException
	 *             if we can't access to the index.
	 * @throws LockObtainFailedException
	 *             if we can't open to the index.
	 * @throws CorruptIndexException
	 *             if the index is corrupt.
	 */
	public void indexFiles(String dirPath, String description, String tags,
			String group) throws CorruptIndexException,
			LockObtainFailedException, IOException;

	/**
	 * Step 2a: generates the content query. Executes a query.
	 * 
	 * @param stringQuery
	 *            the query to use.
	 * @param maxHits
	 *            maximum number of results to get.
	 * @throws ParseException
	 *             if the index is corrupted.
	 * @throws CorruptIndexException
	 *             if the index is corrupted.
	 * @throws IOException
	 *             if we can't access to the index.
	 * @return the results of the query.
	 */
	public Collection<String> queryContents(String stringQuery, int maxHits)
			throws ParseException, CorruptIndexException, IOException;

	/**
	 * Step 2b: generates the description query. Executes a query.
	 * 
	 * @param queryString
	 *            the String we will search for.
	 * @param maxHits
	 *            maximum number of results to get.v
	 * @throws ParseException
	 *             if the index is corrupted.
	 * @throws CorruptIndexException
	 *             if the index is corrupted.
	 * @throws IOException
	 *             if we can't access to the index.
	 * @return the results of the query.
	 * 
	 */
	public Collection<String> queryDescription(String queryString, int maxHits)
			throws CorruptIndexException, IOException, ParseException,
			CorruptIndexException, IOException;

	/**
	 * Step 2c: generates the group query. Executes a query.
	 * 
	 * @param queryString
	 *            the String we will search for.
	 * @param maxHits
	 *            maximum number of results to get.v
	 * @throws ParseException
	 *             if the index is corrupted.
	 * @throws CorruptIndexException
	 *             if the index is corrupted.
	 * @throws IOException
	 *             if we can't access to the index.
	 * @return the results of the query.
	 * 
	 */
	public Collection<String> queryGroup(String queryString, int maxHits)
			throws ParseException, CorruptIndexException, IOException;

	/**
	 * Step 2d: generates the id query. Executes a query.
	 * 
	 * @param queryString
	 *            the String we will search for.
	 * @param maxHits
	 *            maximum number of results to get.
	 * @throws ParseException
	 *             if the index is corrupted.
	 * @throws CorruptIndexException
	 *             if the index is corrupted.
	 * @throws IOException
	 *             if we can't access to the index.
	 * @return the results of the query.
	 * 
	 */
	public Collection<String> queryId(String queryString, int maxHits)
			throws ParseException, CorruptIndexException, IOException;

	/**
	 * Step 2e: generates the tags query. Executes a query.
	 * 
	 * @param queryString
	 *            the String we will search for.
	 * @param maxHits
	 *            maximum number of results to get.
	 * @throws ParseException
	 *             if the index is corrupted.
	 * @throws CorruptIndexException
	 *             if the index is corrupted.
	 * @throws IOException
	 *             if we can't access to the index.
	 * @return the results of the query.
	 * 
	 */
	public Collection<String> queryTags(String queryString, int maxHits)
			throws ParseException, CorruptIndexException, IOException;

	/**
	 * Step 2f: generates the title query. Executes a query.
	 * 
	 * @param queryString
	 *            the String we will search for.
	 * @param maxHits
	 *            maximum number of results to get.
	 * @throws ParseException
	 *             if the index is corrupted.
	 * @throws CorruptIndexException
	 *             if the index is corrupted.
	 * @throws IOException
	 *             if we can't access to the index.
	 * @return the results of the query.
	 * 
	 */
	public Collection<String> queryTitle(String queryString, int maxHits)
			throws ParseException, CorruptIndexException, IOException;

	/**
	 * Adds or updates a new element to the index.
	 * 
	 * @param title
	 *            the document title.
	 * @param description
	 *            the document description.
	 * @param tags
	 *            the document tags.
	 * @param filePath
	 *            the path of the file, where it's stored.
	 * @param group
	 *            the user group whose it belongs.
	 * @throws IOException
	 *             if there's an error in an input/output process.
	 */
	public void addDoc(String title, String description, String tags,
			String filePath, String group) throws IOException;

	/**
	 * Removes every element form the index.
	 * 
	 * @throws IOException
	 *             if there's an error in an input/output process.
	 * @throws CorruptIndexException
	 *             if the index is corrupt.
	 * @throws LockObtainFailedException
	 *             if we couldn't access to the file because it was protected.
	 */
	public void removeAllDocs() throws CorruptIndexException,
			LockObtainFailedException, IOException;

	/**
	 * Removes an element to the index.
	 * 
	 * @param title
	 *            the document title.
	 * @param group
	 *            the group who owns the document.
	 * @throws IOException
	 *             if there's an error in an input/output process.
	 */
	public void removeDoc(String title, String group) throws IOException;

}
