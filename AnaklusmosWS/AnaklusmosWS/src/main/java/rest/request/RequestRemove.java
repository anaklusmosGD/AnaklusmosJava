package rest.request;

/**
 * Simple POJO for Request.
 * 
 * @author Mª Angeles Broullón Lozano.
 *
 */
public class RequestRemove {

	/**
	 * The document title.
	 */
	private String title;
	/**
	 * The group who owns the document.
	 */
	private String group;

	/**
	 * Gets the document title.
	 * 
	 * @return the document title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Updates the document title.
	 * 
	 * @param title
	 *            the new document title.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * The document group.
	 * 
	 * @return gets the document group.
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * Updates the document group.
	 * 
	 * @param group
	 *            the new document group.
	 */
	public void setGroup(String group) {
		this.group = group;
	}
}
