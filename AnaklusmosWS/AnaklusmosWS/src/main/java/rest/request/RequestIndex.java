package rest.request;

/**
 * Simple POJO for Request.
 * 
 * @author Mª Angeles Broullón Lozano.
 *
 */
public class RequestIndex {

	/**
	 * The path of the directory to index.
	 */
	private String dirPath;
	/**
	 * Some description to index the files.
	 */
	private String description;
	/**
	 * Some tags to index the files.
	 */
	private String tags;
	/**
	 * The group whose it belongs.
	 */
	private String group;

	/**
	 * Gets the directory path.
	 * 
	 * @return the directory path.
	 */
	public String getDirPath() {
		return dirPath;
	}

	/**
	 * Sets the directory path.
	 * 
	 * @param dirPath
	 *            the new directory path.
	 */
	public void setDirPath(String dirPath) {
		this.dirPath = dirPath;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * The description.
	 * 
	 * @param description
	 *            the new description.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets new tags.
	 * 
	 * @return the new tags.
	 */
	public String getTags() {
		return tags;
	}

	/**
	 * Sets the new tags.
	 * 
	 * @param tags
	 *            the new tags.
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}

	/**
	 * Gets the group.
	 * 
	 * @return the group.
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * Sets the group.
	 * 
	 * @param group
	 *            the new group.
	 */
	public void setGroup(String group) {
		this.group = group;
	}

}
