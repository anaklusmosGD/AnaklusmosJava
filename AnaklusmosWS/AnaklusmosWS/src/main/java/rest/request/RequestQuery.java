package rest.request;

/**
 * Simple POJO for Request.
 * 
 * @author Mª Angeles Broullón Lozano.
 *
 */
public class RequestQuery {

	/**
	 * What you are looking for.
	 */
	private String stringQuery;
	/**
	 * Maximum number of results to get.
	 */
	private int maxHits;

	/**
	 * What you are looking for.
	 * 
	 * @return the query.
	 */
	public String getStringQuery() {
		return stringQuery;
	}

	/**
	 * Updates the query.
	 * 
	 * @param stringQuery
	 *            the new query.
	 */
	public void setStringQuery(String stringQuery) {
		this.stringQuery = stringQuery;
	}

	/**
	 * Gets the maximum number of results to get.
	 * @return the maximum number of results to get.
	 */
	public int getMaxHits() {
		return maxHits;
	}

	/**
	 * Updates the maximum number of results to get.
	 * @param maxHits the maximum number of results to get.
	 */
	public void setMaxHits(int maxHits) {
		this.maxHits = maxHits;
	}

}
