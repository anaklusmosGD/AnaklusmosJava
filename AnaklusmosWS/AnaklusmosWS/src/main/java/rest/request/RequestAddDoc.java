package rest.request;

/**
 * Simple POJO for Request.
 * 
 * @author Mª Angeles Broullón Lozano.
 *
 */
public class RequestAddDoc {

	/**
	 * The new document title.
	 */
	private String title;
	/**
	 * Some description to index the files.
	 */
	private String description;
	/**
	 * Some tags to index the files.
	 */
	private String tags;
	/**
	 * The path of the directory to index.
	 */
	private String filePath;
	/**
	 * The group whose it belongs.
	 */
	private String group;

	/**
	 * Gets the document title
	 * 
	 * @return the document title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Updates the document title
	 * 
	 * @param title
	 *            the new document title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the document description.
	 * 
	 * @return the document description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Updates the document description.
	 * 
	 * @param description
	 *            the new document description.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the document description.
	 * 
	 * @return the document tags.
	 */
	public String getTags() {
		return tags;
	}

	/**
	 * Updates the document tags.
	 * 
	 * @param tags
	 *            the new document tags.
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}

	/**
	 * Gets the path of the directory to index.
	 * 
	 * @return the path of the directory to index.
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * Updates the path of the directory to index.
	 * 
	 * @param filePath
	 *            the new path of the directory to index.
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * Gets the group whose it belongs.
	 * 
	 * @return the group whose it belongs.
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * Updates the group whose it belongs.
	 * 
	 * @param group
	 *            group whose it belongs.
	 */
	public void setGroup(String group) {
		this.group = group;
	}

}
