/**
 * 
 */
package handleFiles.newOffice;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

public class TestNewOfficeHandler extends TestCase {

	private NewOfficeHandler handler;

	private String pathExcelx = this.getClass().getClassLoader()
			.getResource("NewExcelTestFile.xlsx").getPath();
	private String pathPowerpointx = this.getClass().getClassLoader()
			.getResource("NewPowerpointTest.pptx").getPath();
	private String pathWordx = this.getClass().getClassLoader()
			.getResource("NewWordTestFile.docx").getPath();

	@Before
	protected void setUp() throws Exception {
		super.setUp();
		handler = new NewOfficeHandler();
	}

	@Test
	public void testGetFileContent() {
		boolean testDocx = false;
		boolean testPptx = false;
		boolean testXlsx = false;
		try {
			testDocx = handler.getFileContent(pathWordx).contains(
					"This is a new word file");
			testPptx = handler.getFileContent(pathPowerpointx).contains(
					"This is new powerpoint file");
			testXlsx = handler.getFileContent(pathExcelx).contains(
					"This is a new excel file");
		} catch (IOException e) {
			System.err
					.println("There was an error accessing to the test document files.");
		}
		assertTrue(testDocx && testPptx && testXlsx);
	}

	@Test
	public void testIsValidFormat() {
		assertTrue(handler.isValidFormat("file.docx") && handler.isValidFormat("file.pptx")
				&& handler.isValidFormat("file.xlsx") && !handler.isValidFormat("file.pdf"));
	}

}
