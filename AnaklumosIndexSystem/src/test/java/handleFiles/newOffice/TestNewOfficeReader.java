package handleFiles.newOffice;


import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Test;

public class TestNewOfficeReader extends TestCase {
	
	private String pathExcelx = this.getClass().getClassLoader()
			.getResource("NewExcelTestFile.xlsx").getPath();
	private String pathPowerpointx = this.getClass().getClassLoader()
			.getResource("NewPowerpointTest.pptx").getPath();
	private String pathWordx = this.getClass().getClassLoader()
			.getResource("NewWordTestFile.docx").getPath();

	@Test
	public void testReadDocx() {
		String content = "";
		try {
			content = NewOfficeReader.readDocx(pathWordx);
		} catch (IOException e) {
			fail("Couldn't access to the file " + pathWordx);
		}
		assertTrue(content.contains("This is a new word file"));
	}

	@Test
	public void testReadPptx() {
		String content = "";
		try {
			content = NewOfficeReader.readPptx(pathPowerpointx);
		} catch (IOException e) {
			fail("Couldn't access to the file " + pathPowerpointx);
		}
		assertTrue(content.contains("This is new powerpoint file"));
	}

	@Test
	public void testReadXlsx() {
		String content = "";
		try {
			content = NewOfficeReader.readXlsx(pathExcelx);
		} catch (IOException e) {
			fail("Couldn't access to the file " + pathExcelx);
		}
		assertTrue(content.contains("This is a new excel file"));
	}

}
