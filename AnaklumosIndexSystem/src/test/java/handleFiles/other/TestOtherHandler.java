package handleFiles.other;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

public class TestOtherHandler extends TestCase {

	private OtherHandler handler;
	
	private String pathPdf = this.getClass().getClassLoader()
			.getResource("PdfTestFile.pdf").getPath();
	private String pathRtf = this.getClass().getClassLoader()
			.getResource("RtfTestFile.rtf").getPath();
	private String pathTxt = this.getClass().getClassLoader()
			.getResource("TextTestFile.txt").getPath();

	@Before
	protected void setUp() throws Exception {
		super.setUp();
		handler = new OtherHandler();
	}

	@Test
	public void testGetFileContent() {
		boolean testPdf = false;
		boolean testRtf = false;
		boolean testTxt = false;
		try {
			testPdf = handler.getFileContent(pathPdf).contains("This is a pdf test file");
			testRtf = handler.getFileContent(pathRtf).contains("This is a rtf file");
			testTxt = handler.getFileContent(pathTxt).contains("");
		} catch (IOException e) {
			System.err.println("There was an error accessing to the test document files.");
		}
		assertTrue(testPdf && testRtf && testTxt);
	}

	@Test
	public void testIsValidFormat() {
		assertTrue(handler.isValidFormat("file.pdf") && handler.isValidFormat("file.rtf")
				&& handler.isValidFormat("file.txt") && !handler.isValidFormat("file.doc"));
	}

}
