package handleFiles.other;

import java.util.LinkedList;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

public class TestTxtHandler extends TestCase {

	private String path = this.getClass().getClassLoader()
			.getResource("TextTestFile.txt").getPath();

	@Before
	protected void setUp() throws Exception {
		super.setUp();
		TxtHandler.overwrite(path, getListHelloWorld());
	}

	private List<String> getListHelloWorld() {
		List<String> listHello = new LinkedList<String>();
		listHello.add("hello");
		listHello.add("world");
		return listHello;
	}

	private List<String> getListGoodbyeWorld() {
		List<String> listGoodbye = new LinkedList<String>();
		listGoodbye.add("goodbye");
		listGoodbye.add("world");
		return listGoodbye;
	}

	@Test
	public void testRead() {
		List<String> read = new LinkedList<String>();
		read = TxtHandler.read(path);
		assertEquals(read.get(0), "hello");
	}

	@Test
	public void testReadAll() {
		List<String> read = new LinkedList<String>();
		read = TxtHandler.read(path);
		assertEquals(read, getListHelloWorld());
	}

	@Test
	public void testWrite() {
		TxtHandler.write(path, getListHelloWorld());
		List<String> read = new LinkedList<String>();
		read = TxtHandler.read(path);
		List<String> result = getListHelloWorld();
		result.addAll(getListHelloWorld());
		assertEquals(result, read);
	}

	@Test
	public void testOverwrite() {
		TxtHandler.overwrite(path, getListGoodbyeWorld());
		List<String> read = new LinkedList<String>();
		read = TxtHandler.read(path);
		assertEquals(read, getListGoodbyeWorld());
	}

}
