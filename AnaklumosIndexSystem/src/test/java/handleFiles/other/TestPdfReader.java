package handleFiles.other;



import handleFiles.other.PdfReader;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Test;

public class TestPdfReader extends TestCase {

	private String path = this.getClass().getClassLoader()
			.getResource("PdfTestFile.pdf").getPath();
	
	@Test
	public void testExtractPdfText() {
		String text = "no joy :_(";
		try {
			text = PdfReader.extractPdfText(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertEquals(text, "This is a pdf test file.\n");
	}

}
