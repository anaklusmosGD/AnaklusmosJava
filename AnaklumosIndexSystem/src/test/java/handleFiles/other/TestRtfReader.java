package handleFiles.other;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Test;

public class TestRtfReader extends TestCase {

	private String pathRtf = this.getClass().getClassLoader()
			.getResource("RtfTestFile.rtf").getPath();

	@Test
	public void testReadRtf() {
		String content = "";
		try {
			content = RtfReader.readRtf(pathRtf);
		} catch (IOException e) {
			fail("Couldn't access to the file " + pathRtf);
		}
		assertTrue(content.contains("This is a rtf file"));
	}

}
