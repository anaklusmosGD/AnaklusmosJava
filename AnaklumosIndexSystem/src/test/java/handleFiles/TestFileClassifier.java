package handleFiles;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Test;

public class TestFileClassifier extends TestCase {

	private String pathExcel = this.getClass().getClassLoader()
			.getResource("ClassicExcelTestFile.xls").getPath();
	private String pathPowerpoint = this.getClass().getClassLoader()
			.getResource("ClassicPowerpointTestFile.ppt").getPath();
	private String pathWord = this.getClass().getClassLoader()
			.getResource("ClassicWordTestFile.doc").getPath();
	private String pathExcelx = this.getClass().getClassLoader()
			.getResource("NewExcelTestFile.xlsx").getPath();
	private String pathPowerpointx = this.getClass().getClassLoader()
			.getResource("NewPowerpointTest.pptx").getPath();
	private String pathWordx = this.getClass().getClassLoader()
			.getResource("NewWordTestFile.docx").getPath();
	private String pathPresentation = this.getClass().getClassLoader()
			.getResource("OOPresentation.odp").getPath();
	private String pathSpreadSheet = this.getClass().getClassLoader()
			.getResource("OOSpreadsheet.ods").getPath();
	private String pathTextDoc = this.getClass().getClassLoader()
			.getResource("OOTextDoc.odt").getPath();
	private String pathPdf = this.getClass().getClassLoader()
			.getResource("PdfTestFile.pdf").getPath();
	private String pathRtf = this.getClass().getClassLoader()
			.getResource("RtfTestFile.rtf").getPath();
	private String pathTxt = this.getClass().getClassLoader()
			.getResource("TextTestFile.txt").getPath();

	@Test
	public void testGetFileContent() {
		boolean testDoc = false;
		boolean testPpt = false;
		boolean testXls = false;
		boolean testDocx = false;
		boolean testPptx = false;
		boolean testXlsx = false;
		boolean testOdp = false;
		boolean testOds = false;
		boolean testOdt = false;
		boolean testPdf = false;
		boolean testRtf = false;
		boolean testTxt = false;
		try {
			testDoc = FileClassifier.getFileContent(pathWord).contains(
					"This is a classic word file");
			testPpt = FileClassifier.getFileContent(pathPowerpoint).contains(
					"This is a classic powerpoint file");
			testXls = FileClassifier.getFileContent(pathExcel).contains(
					"This is a classic excel file");
			testDocx = FileClassifier.getFileContent(pathWordx).contains(
					"This is a new word file");
			testPptx = FileClassifier.getFileContent(pathPowerpointx).contains(
					"This is new powerpoint file");
			testXlsx = FileClassifier.getFileContent(pathExcelx).contains(
					"This is a new excel file");
			testOdp = FileClassifier.getFileContent(pathPresentation).contains(
					"This is an openofffice presentation file");
			testOds = FileClassifier.getFileContent(pathSpreadSheet).contains(
					"This is an openoffice spreadsheet file");
			testOdt = FileClassifier.getFileContent(pathTextDoc).contains(
					"This is an openoffice text document file");
			testPdf = FileClassifier.getFileContent(pathPdf).contains(
					"This is a pdf test file");
			testRtf = FileClassifier.getFileContent(pathRtf).contains(
					"This is a rtf file");
			testTxt = FileClassifier.getFileContent(pathTxt).contains("");
		} catch (IOException e) {
			System.err
					.println("There was an error accessing to the test document files.");
		}
		assertTrue(testXls && testPpt && testDoc && testXlsx && testPptx
				&& testDocx && testOdp && testOds && testOdt && testPdf
				&& testRtf && testTxt);
	}

	@Test
	public void testIsValidFormat() {
		assertTrue(FileClassifier.isValidFormat("file.xls")
				&& FileClassifier.isValidFormat("file.ppt")
				&& FileClassifier.isValidFormat("file.doc")
				&& FileClassifier.isValidFormat("file.xlsx")
				&& FileClassifier.isValidFormat("file.pptx")
				&& FileClassifier.isValidFormat("file.docx")
				&& FileClassifier.isValidFormat("file.odp")
				&& FileClassifier.isValidFormat("file.ods")
				&& FileClassifier.isValidFormat("file.odt")
				&& FileClassifier.isValidFormat("file.pdf")
				&& FileClassifier.isValidFormat("file.rtf")
				&& FileClassifier.isValidFormat("file.txt"));
	}

}
