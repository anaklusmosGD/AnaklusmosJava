package handleFiles.classicOffice;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

public class TestClassicOfficeHandler extends TestCase {

	private ClassicOfficeHandler handler;
	
	private String pathExcel = this.getClass().getClassLoader()
			.getResource("ClassicExcelTestFile.xls").getPath();
	private String pathPowerpoint = this.getClass().getClassLoader()
			.getResource("ClassicPowerpointTestFile.ppt").getPath();
	private String pathWord = this.getClass().getClassLoader()
			.getResource("ClassicWordTestFile.doc").getPath();

	@Before
	protected void setUp() throws Exception {
		handler = new ClassicOfficeHandler();
		super.setUp();
	}

	@Test
	public void testGetFileContent() {
		boolean testDoc = false;
		boolean testPpt = false;
		boolean testXls = false;
		try {
			testDoc = handler.getFileContent(pathWord).contains("This is a classic word file");
			testPpt = handler.getFileContent(pathPowerpoint).contains("This is a classic powerpoint file");
			testXls = handler.getFileContent(pathExcel).contains("This is a classic excel file");
		} catch (IOException e) {
			System.err.println("There was an error accessing to the test document files.");
		}
		assertTrue(testDoc && testPpt && testXls);
	}

	@Test
	public void testIsValidFormat() {
		assertTrue(handler.isValidFormat("file.doc") && handler.isValidFormat("file.ppt")
				&& handler.isValidFormat("file.xls") && !handler.isValidFormat("file.pdf"));
	}

}
