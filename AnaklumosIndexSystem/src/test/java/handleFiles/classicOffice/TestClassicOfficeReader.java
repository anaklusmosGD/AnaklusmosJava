package handleFiles.classicOffice;


import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Test;

public class TestClassicOfficeReader extends TestCase {
	
	private String pathExcel = this.getClass().getClassLoader()
			.getResource("ClassicExcelTestFile.xls").getPath();
	private String pathPowerpoint = this.getClass().getClassLoader()
			.getResource("ClassicPowerpointTestFile.ppt").getPath();
	private String pathWord = this.getClass().getClassLoader()
			.getResource("ClassicWordTestFile.doc").getPath();

	@Test
	public void testReadDoc() {
		String content = "";
		try {
			content = ClassicOfficeReader.readDoc(pathWord);
		} catch (IOException e) {
			fail("Couldn't access to the file " + pathWord);
		}
		assertTrue(content.contains("This is a classic word file"));
	}

	@Test
	public void testReadPpt() {
		String content = "";
		try {
			content = ClassicOfficeReader.readPpt(pathPowerpoint);
		} catch (IOException e) {
			fail("Couldn't access to the file " + pathPowerpoint);
		}
		assertEquals(content, "This is a classic powerpoint file\n\n");
	}

	@Test
	public void testReadXls() {
		String content = "";
		try {
			content = ClassicOfficeReader.readXls(pathExcel);
		} catch (IOException e) {
			fail("Couldn't access to the file " + pathExcel);
		}
		assertTrue(content.contains("This is a classic excel file"));
	}

}
