package handleFiles.openOffice;


import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

public class TestOpenOfficeReader extends TestCase {
	
	private String pathPresentation = this.getClass().getClassLoader()
			.getResource("OOPresentation.odp").getPath();
	private String pathSpreadSheet = this.getClass().getClassLoader()
			.getResource("OOSpreadsheet.ods").getPath();
	private String pathTextDoc = this.getClass().getClassLoader()
			.getResource("OOTextDoc.odt").getPath();

	@Before
	protected void setUp() throws Exception {
		super.setUp();
	}

	@Test
	public void testReadOpenOffice() {
		boolean testOdp = false;
		boolean testOds = false;
		boolean testOdt = false;
		try {
			testOdp = OpenOfficeReader.readOpenOffice(pathPresentation).contains(
					"This is an openofffice presentation file");
			testOds = OpenOfficeReader.readOpenOffice(pathSpreadSheet).contains(
					"This is an openoffice spreadsheet file");
			testOdt = OpenOfficeReader.readOpenOffice(pathTextDoc).contains(
					"This is an openoffice text document file");
		} catch (IOException e) {
			System.err
					.println("There was an error accessing to the test document files.");
		}
		assertTrue(testOdp && testOds && testOdt);
	}

}
