package handleFiles.openOffice;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

public class TestOpenOfficeHandler extends TestCase {

	private OpenOfficeHandler handler;

	private String pathPresentation = this.getClass().getClassLoader()
			.getResource("OOPresentation.odp").getPath();
	private String pathSpreadSheet = this.getClass().getClassLoader()
			.getResource("OOSpreadsheet.ods").getPath();
	private String pathTextDoc = this.getClass().getClassLoader()
			.getResource("OOTextDoc.odt").getPath();

	@Before
	protected void setUp() throws Exception {
		super.setUp();
		handler = new OpenOfficeHandler();
	}

	@Test
	public void testGetFileContent() {
		boolean testOdp = false;
		boolean testOds = false;
		boolean testOdt = false;
		try {
			testOdp = handler.getFileContent(pathPresentation).contains(
					"This is an openofffice presentation file");
			testOds = handler.getFileContent(pathSpreadSheet).contains(
					"This is an openoffice spreadsheet file");
			testOdt = handler.getFileContent(pathTextDoc).contains(
					"This is an openoffice text document file");
		} catch (IOException e) {
			System.err
					.println("There was an error accessing to the test document files.");
		}
		assertTrue(testOdp && testOds && testOdt);
	}

	@Test
	public void testIsValidFormat() {
		assertTrue(handler.isValidFormat("file.odp") && handler.isValidFormat("file.ods")
				&& handler.isValidFormat("file.odt") && !handler.isValidFormat("file.pdf"));
	}

}
