package search;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

public class TestDocumentToIndex extends TestCase {

	private String path = this.getClass().getClassLoader()
			.getResource("PdfTestFile.pdf").getPath();
	private DocumentToIndex document;

	@Before
	protected void setUp() throws Exception {
		super.setUp();
		document = new DocumentToIndex("PdfTestFile", "A pdf test file",
				"Test, pdf", path, "Test");
	}

	@Test
	public void testGetDescription() {
		assertTrue(document.getDescription().equals("A pdf test file"));
	}

	@Test
	public void testGetGroup() {
		assertTrue(document.getGroup().equals("Test"));
	}

	@Test
	public void testGetId() {
		assertTrue(document.getId().equals("Test-PdfTestFile"));
	}

	@Test
	public void testGetTags() {
		assertTrue(document.getTags().equals("Test, pdf"));
	}

	@Test
	public void testGetTitle() {
		assertTrue(document.getTitle().equals("PdfTestFile"));
	}

	@Test
	public void testSetContent() {
		boolean done = false;
		try {
			document.setContent(path);
		} catch (IOException e) {
		}
		done = document.getContent().contains("This is a pdf test file.");
		assertTrue(done);
	}

	@Test
	public void testSetDescription() {
		document.setDescription("A new pdf description");
		assertTrue(document.getDescription().equals("A new pdf description"));
	}

	@Test
	public void testSetGroup() {
		document.setGroup("A new group");
		assertTrue(document.getGroup().equals("A new group"));
	}

	@Test
	public void testSetTags() {
		document.setTags("NewTags");
		assertTrue(document.getTags().equals("NewTags"));
	}

	@Test
	public void testSetTitle() {
		document.setTitle("My new pdf Title");
		assertTrue(document.getTitle().equals("My new pdf Title"));
	}

	@Test
	public void testToString() {
		System.out.println(document.toString());
		assertTrue(document
				.toString()
				.contains("[ title=PdfTestFile, tags=Test, pdf, description=A pdf test file, group=Test,"
				+ " content=This is a pdf test file.\n, id=Test-PdfTestFile ]"));
	}
}
