package search;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

import junit.framework.TestCase;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.queryparser.classic.ParseException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestSearchSystem extends TestCase {

	private SearchSystem systemLucene;

	private String path = this.getClass().getClassLoader()
			.getResource("TextTestFile.txt").getPath();

	@Before
	protected void setUp() throws Exception {
		super.setUp();
		setPath();
		systemLucene = new SearchSystem();
	}

	@After
	protected void tearDown() throws Exception {
		super.tearDown();
		systemLucene.removeAllDocs();
	}

	private void setPath() {
		path = path.substring(0, path.lastIndexOf("/"));
	}

	@Test
	public void testIndexFiles() {
		Collection<String> listSearched = new TreeSet<String>();
		try {
			systemLucene.indexFiles(path + "", "prueba de ficheros",
					"test, ficheros, ideas locas", "grupo de prueba");
		} catch (CorruptIndexException e) {
			System.err.println("The index is corrupt.");
			e.printStackTrace();
		} catch (LockObtainFailedException e) {
			System.err
					.println("We couldn't access to the index due to access issues.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("We couldn't access to the index.");
			e.printStackTrace();
		}
		try {
			listSearched = systemLucene.queryTags("ideas locas", 10);
		} catch (CorruptIndexException e) {
			System.err.println("The index is corrupt.");
			e.printStackTrace();
		} catch (ParseException e) {
			System.err.println("We had parsing issues with the index.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("We couldn't access to the index.");
			e.printStackTrace();
		}
		// System.out.println(listSearched);
		// System.out.println(getResultList());
		assertTrue(getResultList().containsAll(listSearched));
	}

	private List<String> getResultList() {
		List<String> list = new LinkedList<String>();
		list.add("ClassicExcelTestFile.xls");
		list.add("ClassicPowerpointTestFile.ppt");
		list.add("ClassicWordTestFile.doc");
		list.add("NewExcelTestFile.xlsx");
		list.add("NewPowerpointTest.pptx");
		list.add("NewWordTestFile.docx");
		list.add("OOPresentation.odp");
		list.add("OOSpreadsheet.ods");
		list.add("OOTextDoc.odt");
		list.add("PdfTestFile.pdf");
		list.add("RtfTestFile.rtf");
		list.add("TextTestFile.txt");
		list.add("TestFileInside.txt");
		return list;
	}

	@Test
	public void testQueryContents() {
		addAFile();
		Collection<String> listSearched = new TreeSet<String>();
		try {
			listSearched = systemLucene.queryContents("pdf", 10);
		} catch (CorruptIndexException e) {
			System.err.println("The index is corrupt.");
			e.printStackTrace();
		} catch (ParseException e) {
			System.err
					.println("We couldn't access to the index due to access issues.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("We couldn't access to the index.");
			e.printStackTrace();
		}
		assertTrue(!listSearched.isEmpty());
	}

	@Test
	public void testQueryDescription() {
		addAFile();
		Collection<String> listSearched = new TreeSet<String>();
		try {
			listSearched = systemLucene.queryDescription("a pdf test file", 1);
		} catch (CorruptIndexException e) {
			System.err.println("The index is corrupt.");
			e.printStackTrace();
		} catch (ParseException e) {
			System.err
					.println("We couldn't access to the index due to access issues.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("We couldn't access to the index.");
			e.printStackTrace();
		}
		assertTrue(!listSearched.isEmpty());
	}

	@Test
	public void testQueryGroup() {
		addAFile();
		Collection<String> listSearched = new TreeSet<String>();
		try {
			listSearched = systemLucene.queryGroup("grupo de prueba", 1);
		} catch (CorruptIndexException e) {
			System.err.println("The index is corrupt.");
			e.printStackTrace();
		} catch (ParseException e) {
			System.err.println("We had parsing issues with the index.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("We couldn't access to the index.");
			e.printStackTrace();
		}
		assertTrue(!listSearched.isEmpty());
	}

	@Test
	public void testQueryId() {
		addAFile();
		Collection<String> listSearched = new TreeSet<String>();
		try {
			listSearched = systemLucene.queryId("grupo de prueba-PdfTestFile",
					1);
		} catch (CorruptIndexException e) {
			System.err.println("The index is corrupt.");
			e.printStackTrace();
		} catch (ParseException e) {
			System.err.println("We had parsing issues with the index.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("We couldn't access to the index.");
			e.printStackTrace();
		}
		assertTrue(!listSearched.isEmpty());
	}

	@Test
	public void testQueryTags() {
		addAFile();
		Collection<String> listSearched = new TreeSet<String>();
		try {
			listSearched = systemLucene.queryTags("test", 1);
		} catch (CorruptIndexException e) {
			System.err.println("The index is corrupt.");
			e.printStackTrace();
		} catch (ParseException e) {
			System.err.println("We had parsing issues with the index.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("We couldn't access to the index.");
			e.printStackTrace();
		}
		assertTrue(!listSearched.isEmpty());
	}

	@Test
	public void testQueryTitle() {
		addAFile();
		Collection<String> listSearched = new TreeSet<String>();
		try {
			listSearched = systemLucene.queryTitle("PdfTestFile", 1);
		} catch (CorruptIndexException e) {
			System.err.println("The index is corrupt.");
			e.printStackTrace();
		} catch (ParseException e) {
			System.err.println("We had parsing issues with the index.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("We couldn't access to the index.");
			e.printStackTrace();
		}
		assertTrue(!listSearched.isEmpty());
	}

	@Test
	public void testAddDoc() {
		addAFile();
		Collection<String> listSearched = new TreeSet<String>();
		try {
			listSearched = systemLucene.queryTitle("PdfTestFile", 1);
		} catch (CorruptIndexException e) {
			System.err.println("The index is corrupt.");
			e.printStackTrace();
		} catch (ParseException e) {
			System.err.println("We had parsing issues with the index.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("We couldn't access to the index.");
			e.printStackTrace();
		}
		assertTrue(listSearched.contains("PdfTestFile"));
	}

	private void addAFile() {
		String myPath = this.getClass().getClassLoader()
				.getResource("PdfTestFile.pdf").getPath();
		try {
			systemLucene.addDoc("PdfTestFile", "a pdf test file", "pdf, test",
					myPath, "grupo de prueba");
		} catch (IOException e) {
			System.err.println("Couldn't add the file.");
			e.printStackTrace();
		}
	}

	@Test
	public void testRemoveDoc() {
		addAFile();
		try {
			systemLucene.removeDoc("PdfTestFile", "grupo de prueba");
		} catch (IOException e) {
			System.err.println("Couldn't remove the file.");
			e.printStackTrace();
		}

		Collection<String> listSearched = new TreeSet<String>();
		try {
			listSearched = systemLucene.queryTitle("PdfTestFile", 1);
		} catch (CorruptIndexException e) {
			System.err.println("The index is corrupt.");
			e.printStackTrace();
		} catch (ParseException e) {
			System.err.println("We had parsing issues with the index.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("We couldn't access to the index.");
			e.printStackTrace();
		}
		System.out.println("The contents after removing are " + listSearched);
		assertTrue(listSearched.isEmpty());
	}

}
