package search;

import handleFiles.FileClassifier;

import java.io.IOException;

/**
 * POJO to store the Documents in the index.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 1.0
 * 
 */
public class DocumentToIndex {

	/**
	 * The file content.
	 */
	private String content;
	/**
	 * A description of the file content.
	 */
	private String description;
	/**
	 * The group which can access to the file.
	 */
	private String group;
	/**
	 * An identifier, generated through the title and group fields.
	 */
	private String id;
	/**
	 * Some tags to identify the content.
	 */
	private String tags;
	/**
	 * The file name.
	 */
	private String title;

	/**
	 * Constructor.
	 * 
	 * @param title
	 *            the file name.
	 * @param description
	 *            a description of the file content.
	 * @param tags
	 *            the tags to get an idea of what's in it.
	 * @param filePath
	 *            the file path, to process its information so we can get the
	 *            content.
	 * @param group
	 *            the group whose it belongs.
	 */
	public DocumentToIndex(String title, String description, String tags,
			String filePath, String group) {
		super();
		this.title = title;
		this.description = description;
		this.tags = tags;
		this.content = "";
		try {
			this.content = getContentFromFile(filePath);
		} catch (IOException e) {
		}
		this.group = group;
		setId();
	}

	/**
	 * Gets a string with the file content parsed, so lucene can index it.
	 * 
	 * @param filePath
	 *            the path of the file.
	 * @return all the content ready to parse on lucene.
	 * @throws IOException
	 *             if we couldn't access to the file.
	 */
	private String getContentFromFile(String filePath) throws IOException {
		return FileClassifier.getFileContent(filePath);
	}

	/**
	 * Gets the file content, parsed.
	 * 
	 * @return the file content, parsed for lucene process.
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Gets the file description.
	 * 
	 * @return the file description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Gets the group who has this file credentials.
	 * 
	 * @return the group who has this file credentials.
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * An id, for the lucene index system.
	 * 
	 * @return an id for the lucene index system.
	 */
	public String getId() {
		return id;
	}

	/**
	 * The file tags, to identify the content.
	 * 
	 * @return the file tags, to identify the content.
	 */
	public String getTags() {
		return tags;
	}

	/**
	 * The file name.
	 * 
	 * @return the file name.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets a string with the file content parsed, so lucene can index it.
	 * 
	 * @param filePath
	 *            the path of the file.
	 * @throws IOException
	 *             if we couldn't access to the file.
	 */
	public void setContent(String filePath) throws IOException {
		getContentFromFile(filePath);
	}

	/**
	 * Sets a description of the file content.
	 * 
	 * @param description
	 *            the description of the file content.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	/**
	 * Sets the identifier, generated through the title and group fields.
	 */
	private void setId() {
		this.id = group + "-" + title;
	}

	/**
	 * Sets some tags to identify the content.
	 * 
	 * @param tags
	 *            an string with some tags to identify the content.
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}

	/**
	 * The file name.
	 * 
	 * @param title
	 *            the new file name.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the instance content as a simple String.
	 * 
	 * @return the instance content in a String.
	 */
	@Override
	public String toString() {
		return "DocumentToIndex [ title=" + title + ", tags=" + tags
				+ ", description=" + description + ", group=" + group
				+ ", content=" + content + ", id=" + id + " ]";
	}

}
