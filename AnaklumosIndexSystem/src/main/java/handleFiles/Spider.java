package handleFiles;

import java.io.File;
import java.util.Collection;
import java.util.TreeSet;

import org.apache.log4j.Logger;

/**
 * Class to get the files list to get the file contents from a path.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 2.0
 */
public class Spider {

	/**
	 * The log system.
	 */
	private static Logger log = Logger.getLogger(Spider.class);

	/**
	 * Gets the content from the path directory, and all the subdirectories
	 * inside it.
	 * 
	 * @param path
	 *            the path in which we will search into.
	 * @return a list with the files paths in the directory.
	 */
	public static Collection<String> listContentRecursive(String path) {
		File f = new File(path);
		Collection<String> l = new TreeSet<String>();
		if (f.exists()) {
			File[] files = f.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					Collection<String> list = listContentRecursive(files[i]
							.getAbsolutePath());
					l.addAll(list);
				} else {
					// only adds those we can actually parse.
					if (FileClassifier
							.isValidFormat(files[i].getAbsolutePath())) {
						l.add(files[i].getAbsolutePath());
					}
				}
			}
		} else {
			log.info("Incorrecta path " + path);
		}
		return l;
	}
}
