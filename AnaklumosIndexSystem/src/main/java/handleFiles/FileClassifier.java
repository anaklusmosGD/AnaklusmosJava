package handleFiles;

import handleFiles.classicOffice.ClassicOfficeHandler;
import handleFiles.newOffice.NewOfficeHandler;
import handleFiles.openOffice.OpenOfficeHandler;
import handleFiles.other.OtherHandler;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Controls which kind of file are we inspecting.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 2.0
 */
public class FileClassifier {

	/**
	 * Initializes a list of possible handler.
	 * 
	 * @return a list of possible handler.
	 */
	private static List<IFileHandler> initialize() {
		List<IFileHandler> handlers = new LinkedList<IFileHandler>();
		handlers.add(new ClassicOfficeHandler());
		handlers.add(new NewOfficeHandler());
		handlers.add(new OpenOfficeHandler());
		handlers.add(new OtherHandler());
		return handlers;
	}

	/**
	 * Gets the correct handler to handle the file.
	 * 
	 * @param filePath
	 *            the path of the file we will work with.
	 * @return a handler if there is one which supports the extension, null if
	 *         there isn't one.
	 */
	private static IFileHandler getCorrectHandler(String filePath) {
		IFileHandler handler = null;
		boolean valid = false;
		Iterator<IFileHandler> it = initialize().iterator();
		IFileHandler actualHandler = new ClassicOfficeHandler();
		while (!valid && it.hasNext()) {
			actualHandler = it.next();
			valid = valid || actualHandler.isValidFormat(filePath);
		}
		if (valid){
			handler = actualHandler;
		}
		return handler;
	}

	/**
	 * Gets the file content if format is valid.
	 * 
	 * @param filePath
	 *            the full name of the file.
	 * @return the whole file content.
	 * @throws IOException
	 *             if we couldn't access to the file..
	 */
	public static String getFileContent(String filePath) throws IOException {
		IFileHandler actualHandler = getCorrectHandler(filePath);
		String content = "";
		if (actualHandler != null) {
			content = actualHandler.getFileContent(filePath);
		}
		return content;
	}

	/**
	 * Checks if the format is valid.
	 * 
	 * @param fileName
	 *            the name of the file we will check.
	 * @return true if the format is valid, otherwise returns false.
	 */
	public static boolean isValidFormat(String fileName) {
		IFileHandler actualHandler = getCorrectHandler(fileName);
		boolean valid = false;
		if (actualHandler != null) {
			valid = true;
		}
		return valid;
	}

}
