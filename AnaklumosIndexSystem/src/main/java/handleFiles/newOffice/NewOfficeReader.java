package handleFiles.newOffice;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.xslf.XSLFSlideShow;
import org.apache.poi.xslf.extractor.XSLFPowerPointExtractor;
import org.apache.poi.xssf.extractor.XSSFExcelExtractor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.xmlbeans.XmlException;

/**
 * Contains the text extraction methods for .docx .pptx and .xlsx files. 
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 2.0
 * 
 */
public class NewOfficeReader {

	/**
	 * The log system.
	 */
	private static Logger log = Logger.getLogger(NewOfficeReader.class);

	/**
	 * Reads a new MS Word file (after Office 2007).
	 * 
	 * @param fileName
	 *            the name of the file to process.
	 * @return the file content in a String.
	 * @throws IOException
	 *             if we couldn't access the file.
	 */
	public static String readDocx(String fileName) throws IOException {
		log.info("Parsing MS word extended " + fileName);
		String data = "";
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(fileName);
			XWPFDocument docx = new XWPFDocument(fis);
			XWPFWordExtractor wordxExtractor = new XWPFWordExtractor(docx);
			data = data + wordxExtractor.getText();
			fis.close();
		} catch (FileNotFoundException e) {
			log.error("We couldn't access to the file " + fileName);
			throw new FileNotFoundException("We couldn't access to the file  "
					+ fileName);
		} catch (IOException e) {
			log.error("There was a WordX error while working with " + fileName);
			throw new IOException("There was a Word error while working with "
					+ fileName);
		}
		return data;
	}

	/**
	 * Reads a new MS PowerPoint file (after Office 2007).
	 * 
	 * @param fileName
	 *            the name of the file to process.
	 * @return the file content in a String.
	 * @throws IOException
	 *             if we couldn't access the file.
	 */
	public static String readPptx(String fileName) throws IOException {
		log.info("Parsing MS powerpoint extended " + fileName);
		String data = "";
		InputStream is = new FileInputStream(fileName);
		XSLFSlideShow pptx;
		try {
			pptx = new XSLFSlideShow(fileName);
			XSLFPowerPointExtractor extractor = new XSLFPowerPointExtractor(
					pptx);
			// gets the xml text.
			data = extractor.getText();
		} catch (OpenXML4JException e) {
			log.error("There was a MS Office error while parsing " + fileName);
		} catch (XmlException e) {
			log.error("The was a XML error while parsing " + fileName);
		}
		is.close();

		return data;
	}

	/**
	 * Reads a new MS Excel file (after Office 2007).
	 * 
	 * @param fileName
	 *            the name of the file to process.
	 * @return the file content in a String.
	 * @throws IOException
	 *             if we couldn't access the file.
	 */
	public static String readXlsx(String fileName) throws IOException {
		log.info("Parsing MS excel extended " + fileName);
		String data = "";
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(fileName);
			XSSFExcelExtractor extractor = new XSSFExcelExtractor(
					new XSSFWorkbook(fis));
			data = extractor.getText();
		} catch (IOException e) {
			throw new IOException("Incorrect file type, " + fileName
					+ " isn't xlsx");
		}
		fis.close();
		return data;
	}

}
