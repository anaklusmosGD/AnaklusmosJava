package handleFiles.newOffice;

import handleFiles.IFileHandler;

import java.io.IOException;

/**
 * Class to manage Office files: .docx, .pptx and .xlsx.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 2.0
 * 
 */
public class NewOfficeHandler implements IFileHandler {

	/**
	 * Gets the file content if it belongs to docx, pptx or xlsx format.
	 * 
	 * @param filePath
	 *            the full name of the file.
	 * @return the whole file content.
	 * @throws IOException
	 *             if we couldn't access to the file..
	 */
	public String getFileContent(String filePath) throws IOException {
		String content = "";
		if (filePath.endsWith(".docx")) {
			content = NewOfficeReader.readDocx(filePath);
		} else {
			if (filePath.endsWith(".pptx")) {
				content = NewOfficeReader.readPptx(filePath);
			} else {
				if (filePath.endsWith(".xlsx")) {
					content = NewOfficeReader.readXlsx(filePath);
				}
			}
		}
		return content;
	}

	/**
	 * Checks if the format is valid.
	 * 
	 * @param fileName
	 *            the name of the file we will check.
	 * @return true if the format is valid, otherwise returns false.
	 */
	public boolean isValidFormat(String filePath) {
		return filePath.endsWith(".docx") || filePath.endsWith(".pptx")
				|| filePath.endsWith(".xlsx");
	}

}
