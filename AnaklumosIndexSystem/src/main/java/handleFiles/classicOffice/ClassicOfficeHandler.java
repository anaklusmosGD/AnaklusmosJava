package handleFiles.classicOffice;

import handleFiles.IFileHandler;

import java.io.IOException;

/**
 * Class to manage Office files: .doc, .ppt and .xls.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 2.0
 * 
 */
public class ClassicOfficeHandler implements IFileHandler {

	/**
	 * Gets the file content if it belongs to doc, ppt or xls format.
	 * 
	 * @param filePath
	 *            the full name of the file.
	 * @return the whole file content.
	 * @throws IOException
	 *             if we couldn't access to the file..
	 */
	public String getFileContent(String filePath) throws IOException {
		String content = "";
		if (filePath.endsWith(".doc")) {
			content = ClassicOfficeReader.readDoc(filePath);
		} else {
			if (filePath.endsWith(".ppt")) {
				content = ClassicOfficeReader.readPpt(filePath);
			} else {
				if (filePath.endsWith(".xls")) {
					content = ClassicOfficeReader.readXls(filePath);
				}
			}
		}
		return content;
	}

	/**
	 * Checks if the format is valid.
	 * 
	 * @param fileName
	 *            the name of the file we will check.
	 * @return true if the format is valid, otherwise returns false.
	 */
	public boolean isValidFormat(String filePath) {
		return filePath.endsWith(".doc") || filePath.endsWith(".ppt")
				|| filePath.endsWith(".xls");
	}

}
