package handleFiles.classicOffice;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.apache.poi.hslf.extractor.PowerPointExtractor;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**
 * Contains the text extraction methods for .doc .ppt and .xls files.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 2.0
 * 
 */
public class ClassicOfficeReader {

	/**
	 * The log system.
	 */
	private static Logger log = Logger.getLogger(ClassicOfficeReader.class);

	/**
	 * Reads a classic MS Word file.
	 * 
	 * @param fileName
	 *            the name of the file to process.
	 * @return the file content in a String.
	 * @throws IOException
	 *             if we couldn't access the file.
	 */
	public static String readDoc(String fileName) throws IOException {
		log.info("Parsing MS word " + fileName);
		String data = "";
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(fileName);
			HWPFDocument word = new HWPFDocument(fis);
			data = data + word.getDocumentText();
			fis.close();
		} catch (FileNotFoundException e) {
			log.error("We couldn't access to the file " + fileName);
			throw new FileNotFoundException("We couldn't access to the file  "
					+ fileName);
		} catch (IOException e) {
			log.error("There was a Word error while working with " + fileName);
			throw new IOException("There was a Word error while working with "
					+ fileName);
		}
		return data;
	}

	/**
	 * Reads a classic MS PowerPoint file.
	 * 
	 * @param fileName
	 *            the name of the file to process.
	 * @return the file content in a String.
	 * @throws IOException
	 *             if we couldn't access the file.
	 */
	public static String readPpt(String fileName) throws IOException {
		log.info("Parsing MS powerpoint " + fileName);
		String data = "";
		InputStream is = new FileInputStream(fileName);
		PowerPointExtractor extractor = new PowerPointExtractor(is);
		// gets the normal and annotations text.
		data = extractor.getText(true, true);
		data = data + extractor.getNotes();
		is.close();
		return data;
	}

	/**
	 * Reads a classic MS Excel file.
	 * 
	 * @param fileName
	 *            the name of the file to process.
	 * @return the file content in a String.
	 * @throws IOException
	 *             if we couldn't access the file.
	 */
	public static String readXls(String fileName) throws IOException {
		log.info("Parsing MS excel " + fileName);
		String data = "";
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(fileName);
			// creates an excel book.
			HSSFWorkbook workbook = new HSSFWorkbook(fis);
			int numberOfSheets = workbook.getNumberOfSheets();
			for (int i = 0; i < numberOfSheets; i++) {
				HSSFSheet sheet = workbook.getSheetAt(i);
				int numberOfRows = sheet.getPhysicalNumberOfRows();
				if (numberOfRows > 0) {
					if (workbook.getSheetName(i) != null
							&& workbook.getSheetName(i).length() != 0) {
						// appending the sheet name to content
						if (i > 0) {
							data = data + ("\n\n");
						}
						data = data + (workbook.getSheetName(i).trim());
						data = data + (":\n");
					}
					Iterator<Row> rowIt = sheet.rowIterator();
					while (rowIt.hasNext()) {
						Row row = rowIt.next();
						if (row != null) {
							boolean hasContent = false;
							Iterator<Cell> it = row.cellIterator();
							while (it.hasNext()) {
								Cell cell = it.next();
								String text = null;
								switch (cell.getCellType()) {
								case HSSFCell.CELL_TYPE_BLANK:
								case HSSFCell.CELL_TYPE_ERROR:
									// ignore all blank or error cells
									break;
								case HSSFCell.CELL_TYPE_NUMERIC:
									text = Double.toString(cell
											.getNumericCellValue());
									break;
								case HSSFCell.CELL_TYPE_BOOLEAN:
									text = Boolean.toString(cell
											.getBooleanCellValue());
									break;
								case HSSFCell.CELL_TYPE_STRING:
								default:
									text = cell.getStringCellValue();
									break;
								}
								if ((text != null) && (text.length() != 0)) {
									data = data + (text.trim());
									data = data + (' ');
									hasContent = true;
								}
							}
							if (hasContent) {
								data = data + ('\n');
							}
						}
					}
				}
			}
			fis.close();
		} catch (FileNotFoundException e) {
			log.error("We couldn't access to the file " + fileName);
			throw new FileNotFoundException("We couldn't access to the file "
					+ fileName);
		}
		return data;
	}

}
