package handleFiles.openOffice;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.w3c.dom.DOMException;

/**
 * Contains the text extraction methods for .ods .odp and .odt files. 
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 2.0
 * 
 */
public class OpenOfficeReader {

	/**
	 * The log system.
	 */
	private static Logger log = Logger.getLogger(OpenOfficeReader.class);

	/**
	 * Reads an Open Office file.
	 * 
	 * @param fileName
	 *            the name of the file to process.
	 * @return the file content in a String.
	 * @throws IOException
	 *             if we couldn't access the file.
	 */
	public static String readOpenOffice(String fileName) throws IOException {
		String data = "";
		try {
			OdfDocument document = OdfDocument.loadDocument(fileName);
			data = document.getContentRoot().getTextContent();
		} catch (IOException e) {
			log.error("There was an OpenOffice error while working with "
					+ fileName);
			throw new IOException(
					"There was an OpenOffice error while working with "
							+ fileName);
		} catch (DOMException e) {
			log.error("There was an OpenOffice error while working with DOM at "
					+ fileName);
			throw new IOException(
					"There was an OpenOffice error while working with "
							+ fileName);
		} catch (Exception e) {
			log.error("There was an OpenOffice error while working with "
					+ fileName + " at the load process");
			throw new IOException(
					"There was an OpenOffice error while working with "
							+ fileName);
		}
		return data;
	}
}
