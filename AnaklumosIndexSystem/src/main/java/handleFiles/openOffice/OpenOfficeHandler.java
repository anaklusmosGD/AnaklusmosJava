package handleFiles.openOffice;

import handleFiles.IFileHandler;

import java.io.IOException;

/**
 * Class to manage Office files: .ods, .pdp and .odt.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 2.0
 * 
 */
public class OpenOfficeHandler implements IFileHandler {

	/**
	 * Gets the file content if it belongs to odp, ods or odt format.
	 * 
	 * @param filePath
	 *            the full name of the file.
	 * @return the whole file content.
	 * @throws IOException
	 *             if we couldn't access to the file..
	 */
	public String getFileContent(String filePath) throws IOException {
		String content = "";
		if (isValidFormat(filePath)) {
			content = OpenOfficeReader.readOpenOffice(filePath);
		}
		return content;
	}

	/**
	 * Checks if the format is valid.
	 * 
	 * @param fileName
	 *            the name of the file we will check.
	 * @return true if the format is valid, otherwise returns false.
	 */
	public boolean isValidFormat(String filePath) {
		return filePath.endsWith(".odp") || filePath.endsWith(".ods")
				|| filePath.endsWith(".odt");
	}

}
