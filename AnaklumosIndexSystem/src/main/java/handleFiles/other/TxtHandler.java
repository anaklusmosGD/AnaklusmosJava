package handleFiles.other;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Class to quickly manage txt files.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 2.0
 * 
 */
public class TxtHandler {
	
	/**
	 * The log system.
	 */
	private static Logger log = Logger.getLogger(TxtHandler.class);

	/**
	 * Reads the file content line by line, saving each one of them in an item
	 * list.
	 * 
	 * @param path
	 *            the path of the file we want to read.
	 * @return the content, one line per Strings.
	 */
	public static List<String> read(String path) {
		List<String> content = new LinkedList<String>();
		File file = new File(path);
		FileReader fr = null;
		BufferedReader br = null;
		String read = "";

		// Reads the file
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			read = br.readLine();
			while (read != null) {
				content.add(read);
				read = br.readLine();
			}
		} catch (FileNotFoundException e) {
			log.error("There was an error accesing to the file.");
			log.error(e.getStackTrace());
		} catch (Exception e) {
			log.error("There was an error while reading the file.");
			log.error(e.getStackTrace());
		} finally {
			// we close the file
			try {
				if (fr != null) {
					fr.close();
				}
			} catch (Exception e) {
				log.error("We couldn't close the file.");
			}

		}
		return content;
	}

	/**
	 * Reads the file content line by line, saving all of it in single string.
	 * 
	 * @param path
	 *            the path of the file we want to read.
	 * @return the whole file content in a single String.
	 */
	public static String readAll(String path) {
		log.info("Parsing the file " + path);
		String content = "";
		List<String> listContent = read(path);
		for (int i = 0; i < listContent.size(); i++) {
			content = content + listContent.get(i) + "\n";
		}
		return content;
	}

	/**
	 * Writes the content of a list of strings, after what was previously
	 * written in the file.
	 * 
	 * @param path
	 *            the path of the file we want to read.
	 * @param input
	 *            the list of text lines we want to write in the file.
	 */
	public static void write(String path, List<String> input) {
		// handles the file and wrting pointer
		FileWriter file = null;
		PrintWriter pw = null;
		Iterator<String> it = input.iterator();
		try {
			// adds new lines to what was previous there.
			file = new FileWriter(path, true);
			pw = new PrintWriter(file, true);
			while (it.hasNext()) {
				pw.println(it.next());
			}
		} catch (FileNotFoundException e) {
			log.error("There was an access error.");
			log.error(e.getStackTrace());
		} catch (IOException e) {
			log.error("There was an error while we tried to read.");
			log.error(e.getStackTrace());
		} finally {
			// we are checking that the file is closed.
			try {
				if (null != file) {
					file.close();
				}
			} catch (Exception e) {
				log.error("There was an error while closing the file.");
			}
		}
	}

	/**
	 * Writes the content of a list of strings, deleting all that was previously
	 * written in the file.
	 * 
	 * @param path
	 *            the path of the file we want to read.
	 * @param input
	 *            the list of text lines we want to write in the file.
	 */
	public static void overwrite(String path, List<String> input) {
		// handles the file and wrting pointer
		FileWriter file = null;
		PrintWriter pw = null;
		Iterator<String> it = input.iterator();
		try {
			// add the text
			file = new FileWriter(path);
			pw = new PrintWriter(file, true);
			while (it.hasNext()) {
				pw.println((String) it.next());
			}
		} catch (FileNotFoundException e) {
			log.error("There was an access error.");
			log.error(e.getStackTrace());
		} catch (IOException e) {
			log.error("There was an error while we tried to read.");
			log.error(e.getStackTrace());
		} finally {
			// we are checking that the file is closed.
			try {
				if (null != file) {
					file.close();
				}
			} catch (Exception e) {
				log.error("There was an error while closing the file.");
			}
		}
	}
}
