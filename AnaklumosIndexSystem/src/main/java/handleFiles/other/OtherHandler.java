package handleFiles.other;

import handleFiles.IFileHandler;

import java.io.IOException;

/**
 * Class to quickly manage pdf, rtd and txt files.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 2.0
 * 
 */
public class OtherHandler implements IFileHandler {
	
	/**
	 * Gets the file content if it belongs to docx, pptx or xlsx format.
	 * 
	 * @param filePath
	 *            the full name of the file.
	 * @return the whole file content.
	 * @throws IOException
	 *             if we couldn't access to the file..
	 */
	public String getFileContent(String filePath) throws IOException {
		String content = "";
		if (filePath.endsWith(".pdf")) {
			content = PdfReader.extractPdfText(filePath);
		} else {
			if (filePath.endsWith(".rtf")) {
				content = RtfReader.readRtf(filePath);
			} else {
				if (filePath.endsWith(".txt")) {
					content = TxtHandler.readAll(filePath);
				}
			}
		}
		return content;
	}

	/**
	 * Checks if the format is valid.
	 * 
	 * @param fileName
	 *            the name of the file we will check.
	 * @return true if the format is valid, otherwise returns false.
	 */
	public boolean isValidFormat(String filePath) {
		return filePath.endsWith(".pdf") || filePath.endsWith(".rtf")
				|| filePath.endsWith(".txt");
	}

}
