package handleFiles.other;

import java.io.FileInputStream;
import java.io.IOException;

import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.rtf.RTFEditorKit;

import org.apache.log4j.Logger;

/**
 * Class to quickly manage rtf files.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 2.0
 * 
 */
public class RtfReader {
	
	/**
	 * The log system.
	 */
	private static Logger log = Logger.getLogger(RtfReader.class);
	
	/**
	 * Reads an RTF file.
	 * 
	 * @param fileName
	 *            the name of the file to process.
	 * @return the file content in a String.
	 * @throws IOException
	 *             if we couldn't access the file.
	 */
	public static String readRtf(String fileName) throws IOException {
		log.info("Parsing RTF " + fileName);
		String data = "";
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(fileName);
			DefaultStyledDocument styledDoc = new DefaultStyledDocument();
			new RTFEditorKit().read(fis, styledDoc, 0);
			data = styledDoc.getText(0, styledDoc.getLength());
		} catch (IOException e) {
			throw new IOException("Incorrect file type, " + fileName
					+ " isn't rtf");
		} catch (BadLocationException e) {
			throw new IOException("Incorrect file location, " + fileName);
		}
		fis.close();
		return data;
	}

}
