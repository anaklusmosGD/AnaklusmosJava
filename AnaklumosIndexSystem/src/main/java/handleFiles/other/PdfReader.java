package handleFiles.other;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

/**
 * Class which controls the the reading process for Pdf files. Uses PDFBox:
 * http://pdfbox.apache.org/
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 2.0
 */
public class PdfReader {

	/**
	 * The log system.
	 */
	private static Logger log = Logger.getLogger(PdfReader.class);

	/**
	 * Extracts the text form the pdf and puts it in a String.
	 * 
	 * @param path
	 *            the path of the file whose contents we want to get.
	 * @return a String with the pdf contents.
	 * @throws IOException
	 *             if we can't open the file, or we can't parse it.
	 */
	public static String extractPdfText(String path) throws IOException {
		log.info("Parsing pdf from " + path);
		String parsedText = "";

		File f = new File(path);
		if (!f.isFile()) {
			log.error("The file " + path + " doesn't exist.");
			return "";
		}

		if (path.endsWith(".pdf")) {
			PDFParser parser = new PDFParser(new FileInputStream(f));
			parser.parse();
			COSDocument cosDoc = parser.getDocument();
			PDFTextStripper pdfStripper = new PDFTextStripper();
			PDDocument pdDoc = new PDDocument(cosDoc);
			parsedText = pdfStripper.getText(pdDoc);
			cosDoc.close();
		} else {
			throw new IOException("Incorrect file type, " + path + " isn't pdf");
		}
		return parsedText;
	}
}
