package interfaz;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import org.apache.lucene.queryParser.ParseException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;

import buscador.Buscador;


/**
 * Clase que controla la interfaz gr�fica.
 * @author Mª Angeles Broullón Lozano.
 * @version 0.1
 */
public class Controlador implements ActionListener{
	
	//ATRIBUTOS
	/**
	 * La ventana que controlará.
	 */
	private Ventana ventanaControlada;
	/**
	 * El buscador con el que trabajar.
	 */
	private static Buscador busq;
	

	//M�TODOS	
	/**
	 * Constructor con parámetro.
	 * @param win la ventana que será controlada mediante esta instancia.
	 */
	public Controlador(Ventana win){
		ventanaControlada = win;
		busq = new Buscador();		
	}
	
	/**
	 * Bloquea los botones de la ventana.
	 */
	public void bloquearBotones(){
		ventanaControlada.iniciarBuscaUno.setEnabled(false);
		ventanaControlada.iniciarBuscaTodos.setEnabled(false);
		ventanaControlada.iniciarBuscaDiez.setEnabled(false);
	}
	
	/**
	 * Desbloquea los botones de la ventana.
	 */
	public void desbloquearBotones(){
		ventanaControlada.iniciarBuscaUno.setEnabled(true);
		ventanaControlada.iniciarBuscaTodos.setEnabled(true);
		ventanaControlada.iniciarBuscaDiez.setEnabled(true);
	}
		
	//métodos de ActionListener
	/**
	 * El método de control en sí.
	 * @param e el evento que queremos controlar.
	 */
	public void actionPerformed(ActionEvent e) {
		//cambiamos el cursor por un reloj
		Cursor cur = new Cursor(Cursor.WAIT_CURSOR);
        ventanaControlada.setCursor(cur); 
        //y creamos un cronometro
        Cronometro cronometro = new Cronometro();

        //ejecutamos la acción correspondiente    	
    	if (e.getSource().equals(ventanaControlada.iniciarBuscaTodos)){
    		//aqui irá el principal:
    		String entrada = ventanaControlada.getEntrada();
    		//ahora recorremos "lista" para buscar "entrada"
    		System.out.println("Buscamos la palabra: " + entrada);
    		 try {
				busq.consultarM(entrada);
			} catch (IOException e1) {
				System.err.println("Se ha producido un error de entrada/salida.");
				e1.printStackTrace();
			} catch (ParseException e1) {
				System.err.println("Se ha producido un error de parseado.");
				e1.printStackTrace();
			}
    		//Busca bus = new Busca();
    		String encontrados = "Está en los archivos:\n";
    		ventanaControlada.setSalida(encontrados);
    		try {
				encontrados=busq.mostrarResultados();
			} catch (IOException e1) {
				System.err.println("Se ha producido un error de entrada/salida.");
				e1.printStackTrace();
			}
    		//y volcamos la salida a pantalla.
    		//ventanaControlada.setSalida(encontrados + "Fin de la búsqueda: aparece en " + " fuentes.");
    		double tiempo = cronometro.mirarCronometro();
    		
    		String salida = encontrados + "Fin de la búsqueda: aparece en " + " fuentes.\n";
    		tiempo = tiempo/1000000000;
    		ventanaControlada.setSalida(salida + "Tiempo de ejecución: " + tiempo + " segundos");
    	}else{
    		//ejecutamos la acción correspondiente    	
        	if (e.getSource().equals(ventanaControlada.iniciarBuscaUno)){
        		//aqui irá el principal:
        		String entrada = ventanaControlada.getEntrada();
        		//ahora recorremos "lista" para buscar "entrada"
        		System.out.println("Buscamos la palabra: " + entrada);
        		 try {
    				busq.consultarU(entrada);
    			} catch (IOException e1) {
    				System.err.println("Se ha producido un error de entrada/salida.");
    				e1.printStackTrace();
    			} catch (ParseException e1) {
    				System.err.println("Se ha producido un error de parseado.");
    				e1.printStackTrace();
    			}
        		//Busca bus = new Busca();
        		String encontrados = "Está en los archivos:\n";
        		ventanaControlada.setSalida(encontrados);
        		try {
    				encontrados=busq.mostrarResultados();
    			} catch (IOException e1) {
    				System.err.println("Se ha producido un error de entrada/salida.");
    				e1.printStackTrace();
    			}
        		//y volcamos la salida a pantalla.
        		//ventanaControlada.setSalida(encontrados + "Fin de la búsqueda: aparece en " + " fuentes.");
        		double tiempo = cronometro.mirarCronometro();
        		
        		String salida = encontrados + "Fin de la búsqueda: aparece en " + " fuentes.\n";
        		tiempo = tiempo/1000000000;
        		ventanaControlada.setSalida(salida + "Tiempo de ejecución: " + tiempo + " segundos");
        
        	}else{
        		if (e.getSource().equals(ventanaControlada.iniciarBuscaDiez)){
        			//aqui irá el principal:
            		String entrada = ventanaControlada.getEntrada();
            		//ahora recorremos "lista" para buscar "entrada"
            		System.out.println("Buscamos la palabra: " + entrada);
            		 try {
        				busq.consultar(entrada);
        			} catch (IOException e1) {
        				System.err.println("Se ha producido un error de entrada/salida.");
        				e1.printStackTrace();
        			} catch (ParseException e1) {
        				System.err.println("Se ha producido un error de parseado.");
        				e1.printStackTrace();
        			}
            		//Busca bus = new Busca();
            		String encontrados = "Está en los archivos:\n";
            		ventanaControlada.setSalida(encontrados);
            		try {
        				encontrados=busq.mostrarResultados();
        			} catch (IOException e1) {
        				System.err.println("Se ha producido un error de entrada/salida.");
        				e1.printStackTrace();
        			}
            		//y volcamos la salida a pantalla.
            		//ventanaControlada.setSalida(encontrados + "Fin de la búsqueda: aparece en " + " fuentes.");
            		double tiempo = cronometro.mirarCronometro();
            		
            		String salida = encontrados + "Fin de la bçusqueda: aparece en " + " fuentes.\n";
            		tiempo = tiempo/1000000000;
            		ventanaControlada.setSalida(salida + "Tiempo de ejecución: " + tiempo + " segundos");
        	    }
        	}
    	}
		
		//dejamos el cursor como estaba
		cur = new Cursor(Cursor.DEFAULT_CURSOR);
		//ventanaControlada.desbloquearBotones();
        ventanaControlada.setCursor(cur);        
    }
	
	/**
	 * El m�todo principal.
	 * @param args irrelevante en este caso.
	 */
	@SuppressWarnings("deprecation")
	public static void main(String args[]){
		System.out.println("Inicializando el buscador.");				
		//creamos la ventana y la vista
		Ventana v = new Ventana();
		
		DialogFileChooser d = new DialogFileChooser(v);
		d.show();		
		
		Controlador c = new Controlador(v);
		String  ruta = v.getRuta();
		
		if (ruta.equals("...Salir_del_programa")){
			v.dispose();
		}else{
			v.añadirControlador(c);		
			v.crearVista();
			try {
				c.bloquearBotones();
				c.ventanaControlada.setFuenteSalida2();
				c.ventanaControlada.setSalida("Inicializando, espere un poco.");
				//TODO
				busq.indexar(ruta);
				c.ventanaControlada.setFuenteSalida();
				c.ventanaControlada.setSalida("Aún no ha introducido ninguna consulta.");
				c.desbloquearBotones();
			} catch (IOException e) {
				System.err.append("No se pudo inicializar la aplicación.");
				e.printStackTrace();
			} catch (OpenXML4JException e) {
				System.err.append("No se pudo indexar un pptx.");
				e.printStackTrace();
			}	
		}
		
		
	}

}




