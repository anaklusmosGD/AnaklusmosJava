package interfaz;

/**
 * Clase para calcular el tiempo de ejecución.
 * @author Mª Angeles Broullón Lozano.
 * @version 1.0
 */
public class Cronometro {
	//ATRIBUTOS
	/**
	 * El momento de inicio de nuestro programa.
	 */
	private long inicio;
	
	/**
	 * El momento en que finaliza nuestro programa.
	 */
	private long fin;
	
	//METODOS	
	/**
	 * Constructor sin parámetros, que inicializa el cronómetro.
	 */
	public Cronometro(){
		resetear();
	}
	
	/**
	 * Método que "pone a 0 el cronometro". Lo que hace es tomar el tiempo del reloj interno de la CPU.	
	 */
	public void resetear(){
		inicio = System.nanoTime();
	}
	
	/**
	 * Método que devuelve el tiempo en nanosegundos que ha transcurrido desde que reseteamos el cronómetro.
	 * @return el tiempo en nanosegundos que tarde la ejecución.
	 */
	public long mirarCronometro(){
		fin = System.nanoTime();
		return (fin-inicio);
	}
	

}
