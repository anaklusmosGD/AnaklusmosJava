package interfaz;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.TextArea;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;


/**
 * Clase que define la interfaz gráfica.
 * @author Mª Angeles Broullón Lozano.
 * @version 0.1
 */
public class Ventana extends JFrame{
	
	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;
	//ATRIBUTOS
	/**
	 * Donde se recogerá la entrada.
	 */
	private TextArea entrada;
	/**
	 * Botón para iniciar la búsqueda obteniendo todas los documentos en los que aparece.
	 */
	protected JButton iniciarBuscaTodos;
	/**
	 * Botón para iniciar la búsqueda obteniendo todas los documentos en los que aparece.
	 */
	protected JButton iniciarBuscaDiez;
	/**
	 * Botón para iniciar la búsqueda obteniendo solo un documentos en el que aparece.
	 */
	protected JButton iniciarBuscaUno;
	/**
	 * Donde se recogerá la salida.
	 */
	private TextArea salida;
	/**
	 * El controlador de la ventana.
	 */
	private Controlador control;
	/**
	 * La ruta en la que buscar.
	 */
	private String ruta;
	
	//METODOS
	//constructor
	/**
	 * Constructor sin par�metros.
	 */
	public Ventana(){
		super();
		this.setResizable(false);
	}
	
	//constuir la vista de ventana
	/**
	 * Construye la ventana, haci�ndola visible.
	 */
	public void crearVista() {
		//crear los elementos
		entrada = new TextArea("Introduzca la palabra o expresión que desea buscar", 1, 40, TextArea.SCROLLBARS_NONE);
		iniciarBuscaTodos = new JButton("Buscar todos");
		iniciarBuscaUno = new JButton("Buscar uno");
		iniciarBuscaDiez = new JButton("Busca diez");
		salida = new TextArea("Aún no se introdujo ninguna consulta",22,40,TextArea.SCROLLBARS_VERTICAL_ONLY );
		salida.setEditable(false);
		Container container = getContentPane();
		setLayout(new FlowLayout());
		//añade los controladores
		iniciarBuscaTodos.addActionListener(control);
		iniciarBuscaUno.addActionListener(control);
		iniciarBuscaDiez.addActionListener(control);
		//añadir los elementos a la ventana			
		container.add(entrada);
		container.add(iniciarBuscaUno);
		container.add(iniciarBuscaDiez);
		container.add(iniciarBuscaTodos);
		container.add(salida);
		
		
		//control de cierre
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
		//visualización
		setSize(320, 500);
		setVisible(true);		
		this.setTitle("Buscador 0.1");
	}
	
	//GETTERS
	/**
	 * Método observador que devuelve el contenido del cuadro de texto de entrada.
	 * @return el contenido del cuadro de texto de entrada.
	 */
	protected String getEntrada(){
		return entrada.getText();
	}
	
	/**
	 * Método observador que devuelve la ruta en donde buscar.
	 * @return la ruta en donde buscar.
	 */
	protected String getRuta(){
		return ruta;
	}
	
	//SETTERS
	/**
	 * Modifica la salida.
	 * @param s la nueva salida.
	 */
	public void setSalida(String s){
		salida.setText(s);
	}
	
	/**
	 * Coloca la fuente de salida en modo normal.
	 */
	public void setFuenteSalida(){
		salida.setFont(new Font("Dialog", Font.PLAIN, 12));
	}
	
	/**
	 * Coloca la fuente de salida en modo negrita.
	 */
	public void setFuenteSalida2(){
		salida.setFont(new Font("Dialog", Font.BOLD, 12));
	}
	
	/**
	 * Establece la nueva ruta de directorio en que queremos indexar.
	 * @param r la nueva ruta de directorio en que queremos indexar.
	 */
	protected void setRuta(String r){
		ruta = r;
	}
	
	/**
	 * Añade el controlador de la ventana.
	 * @param c el controlador a asignarle.
	 */
	public void añadirControlador(Controlador c){
		control = c;
	}
	
}


