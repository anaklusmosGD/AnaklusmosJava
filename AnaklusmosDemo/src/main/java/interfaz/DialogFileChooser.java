package interfaz;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Clase que define el menú de selección directorio en el arranque.
 * @author Mª Angeles Broullón Lozano.
 * @version 0.1
 */
public class DialogFileChooser extends JDialog implements ActionListener {

	// ATRIBUTOS
	/**
	 * El serial por defecto.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * La ventana padre.
	 */
	private Ventana ventanaPadre;
	/**
	 * La etiqueta de ruta.
	 */
	private JLabel etiquetaRuta;
	/**
	 * El área de texto que contendrá la ruta.
	 */
	private TextField ruta;
	/**
	 * Boton para lanzar el sistema de eleccion de directorio.
	 */
	private JButton botonMas;
	/**
	 * El botón para aceptar.
	 */
	private JButton botonAceptar;
	/**
	 * El botón para cancelar.
	 */
	private JButton botonCancelar;

	// MÉTODOS
	/**
	 * Constructor con un parámetro.
	 * 
	 * @param v
	 *            la ventana padre.
	 */
	public DialogFileChooser(Ventana v) {
		super(v, true);
		ventanaPadre = v;
		this.setLayout(new BorderLayout());

		// componentes
		JPanel comp = new JPanel();
		comp.setLayout(new FlowLayout());
		etiquetaRuta = new JLabel("¿Qué directorio desea indexar?");
		comp.add(etiquetaRuta);
		ruta = new TextField("...............................");
		ruta.setText("/home/");
		comp.add(ruta);
		botonMas = new JButton("+");
		comp.add(botonMas);
		botonMas.addActionListener(this);
		this.add(comp, BorderLayout.NORTH);

		// botones
		JPanel botones = new JPanel();
		botones.setLayout(new FlowLayout());
		botonAceptar = new JButton("Aceptar");
		botonAceptar.addActionListener(this);
		botones.add(botonAceptar);
		botonCancelar = new JButton("Cancelar");
		botonCancelar.addActionListener(this);
		botones.add(botonCancelar);
		this.getContentPane().add(botones, BorderLayout.SOUTH);

		// control de cierre
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		this.setSize(500, 100);
		this.setTitle("Buscador 0.1");
	}

	/**
	 * M�todo que controla las acciones de la ventana.
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(botonAceptar)) {
			System.out.println("Acepto");
			ventanaPadre.setRuta(ruta.getText());
			this.dispose();
		} else {
			if (e.getSource().equals(botonCancelar)) {
				System.out.println("Cancelo");
				ventanaPadre.setRuta("...Salir_del_programa");
				this.dispose();
			} else {
				if (e.getSource().equals(botonMas)) {
					System.out.println("Mas");
					JFileChooser fc = new JFileChooser();
					fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					int returnVal = fc.showOpenDialog(this);

					if (returnVal == JFileChooser.APPROVE_OPTION) {
						String r = fc.getSelectedFile().getAbsolutePath();
						ruta.setText(r);
					}
				}
			}
		}

	}

}