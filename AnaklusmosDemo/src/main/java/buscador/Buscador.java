package buscador;

import java.io.File;
import java.io.IOException;
import java.util.List;

import manejoFicheros.ClasificadorDeFicheros;
import manejoFicheros.LectorFormatos;
import manejoFicheros.LectorPdf;
import manejoFicheros.ManejadorFichero;
import manejoFicheros.Spider;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.store.RAMDirectory;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;

/**
 * Clase para la busqueda de palabras en un fichero.
 * @author Mª Angeles Broullón Lozano.
 * @version 0.1
 *
 */
@SuppressWarnings({ "unused", "deprecation" })
public class Buscador {
		
	//ATRIBUTOS
	/**
	 * El analizador que utilizaremos.
	 */
	private StandardAnalyzer analyzer;
	/**
	 * El nuevo índice.
	 */
	private Directory index;
	/**
	 * El índice de búsqueda.
	 */
	private IndexSearcher searcher;
	/**
	 * Aqui alamacenaremos los resultados.
	 */
	private ScoreDoc[] hits;
	
	
	//METODOS
		
	/**
	 * Método que busca en los ficheros la palabra deseada.
	 * @param direccion direccion del archivo en que buscamos.
	 * @param palabra palabra a buscar.
	 * @return encontrado boleano.
	 */
	public boolean buscador(String direccion, String palabra){
		List<String> fichero = ManejadorFichero.leer(direccion);
		//en "fichero" tenemos el contenido de un archivo.
		int i=0;		
		boolean encontrado = false;
		while (i<fichero.size() && !encontrado){
			//Comprobamos que la palabra esta o no			
			if (fichero.get(i).toString().contains(palabra.toString())){
				encontrado = true;
			}
			i++;
		}
		return encontrado;
	}
	
	/**
	 * Método que busca en los ficheros la palabra deseada.
	 * @param direccion direccion del archivo en que buscamos.
	 * @param palabra palabra a buscar.
	 * @return encontrado booleano.
	 */
	public String buscador2(String direccion, String palabra){
		List<String> fichero = ManejadorFichero.leer(direccion);
		int i=0;		
		String frase = "";
		boolean encontrado = false;
		while (i<fichero.size() && !encontrado){
			//Comprobamos que la palabra esta o no			
			if (fichero.get(i).toString().contains(palabra.toString())){
				int j=0;
				for(j=0;j<8;j++){
					frase = frase + fichero.get(i+j);
					System.out.println(frase);
					encontrado = true;
					break;					
				}
			}
			i++;
		}		
		return frase;
	}
	
	/**
	 * Realiza la etapa de indexado.
	 * @param rutaEntrada la ruta del directorio en el que buscar.
	 * @throws IOException si hay problemas de entrada/salida.
	 * @throws OpenXML4JException si se produce un error de acceso a objetos extendidos.
	 */
	public void indexar(String rutaEntrada) throws IOException, OpenXML4JException{
		// 0.Especificar el analizador para el texto
		analyzer = new StandardAnalyzer();
		  
		// 1. crear el �ndice
		IndexWriter w = new IndexWriter(new File("c:/lucene_output_dir_index"), new StandardAnalyzer(), true);  		
		//y a a�adir archivos se ha dicho.
		Spider spider = new Spider();
		List<String> lista = spider.listarRecursivo(rutaEntrada);
		for (int i=0; i<lista.size(); i++){
		   System.out.println(i + "- indexando " + lista.get(i));
		   
		   if (ClasificadorDeFicheros.esTxt(lista.get(i))){
			   String contenido = ManejadorFichero.leerTodo(lista.get(i));
			   addDoc(w, lista.get(i), contenido);
		   }else{
			   if (ClasificadorDeFicheros.esPdf(lista.get(i))){
				   String contenido = LectorPdf.extraerTextoDePdf(lista.get(i));
				   addDoc(w, lista.get(i), contenido);
			   }else{
				   if (ClasificadorDeFicheros.esDoc(lista.get(i))){
					   String contenido = LectorFormatos.leerWord(lista.get(i));
					   addDoc(w, lista.get(i), contenido);
				   }else{
					   if (ClasificadorDeFicheros.esXls(lista.get(i))){
						   String contenido = LectorFormatos.leerExcel(lista.get(i));
						   addDoc(w, lista.get(i), contenido);
					   }else{
						   if (ClasificadorDeFicheros.esPpt(lista.get(i))){
							   String contenido = LectorFormatos.leerPowerPoint(lista.get(i));
							   addDoc(w, lista.get(i), contenido);
						   }else{
							   if (ClasificadorDeFicheros.esVsd(lista.get(i))){
								   String contenido = LectorFormatos.leerVisio(lista.get(i));
								   addDoc(w, lista.get(i), contenido);
							   }else{
								   if (ClasificadorDeFicheros.esPub(lista.get(i))){
									   String contenido = LectorFormatos.leerPublisher(lista.get(i));
									   addDoc(w, lista.get(i), contenido);
								   }else{
									   if (ClasificadorDeFicheros.esMsg(lista.get(i))){
										   String contenido = LectorFormatos.leerOutlook(lista.get(i));
										   addDoc(w, lista.get(i), contenido);
									   }else{
										   if (ClasificadorDeFicheros.esDocx(lista.get(i))){
											   String contenido = LectorFormatos.leerWordX(lista.get(i));
											   addDoc(w, lista.get(i), contenido);
										   }else{
											   if (ClasificadorDeFicheros.esXlsx(lista.get(i))){
												   String contenido = LectorFormatos.leerExcelX(lista.get(i));
												   addDoc(w, lista.get(i), contenido);
											   }else{
												   if (ClasificadorDeFicheros.esPptx(lista.get(i))){
													   String contenido = LectorFormatos.leerPowerPointX(lista.get(i));
													   addDoc(w, lista.get(i), contenido);
												   }
											   }
										   }
									   }
								   }
							   }
						   }
					   }
				   }
			   }
		   }		   
		}
		w.close();
	}
	    
	/**
	 * Añade un nuevo documento al índice.
	 * @param w el índice sobre el que escribiremos.
	 * @param title el titulo del documento.
	 * @param value el nuevo valor a añadir.
	 * @throws IOException si se produce un errr en un proceso de entrada/salida.
	 */
	private static void addDoc(IndexWriter w, String title, String value) throws IOException {
	    Document doc = new Document();
	    doc.add(new Field("titulo", title, Field.Store.YES, Field.Index.ANALYZED));
	    doc.add(new Field("contenido", value, Field.Store.YES, Field.Index.ANALYZED));
	    w.addDocument(doc);
	}
	    
	
	
	/**
	 * Realiza una consulta de 10 resultados.
	 * @throws IOException si se produce un error de entrada/salida.
	 * @throws ParseException si se produce un error al analizar el texto.
	 */
	public void consultar(String s) throws IOException, ParseException{
		// 2. generar consulta.
	    String querystr = s;

	    // el argumento "title" especifica el campo por defecto
	    // a usar cuando no hay ninguno por defecto.
	    QueryParser parser = new QueryParser("contenido", new StandardAnalyzer());  
        Query query = parser.parse(querystr);  
          
	    //Query q = new QueryParser("title", analyzer).parse(querystr);

	    // 3. busqueda
	    int hitsPerPage = 10;
	    searcher = new IndexSearcher("c:/lucene_output_dir_index");
	    TopDocCollector collector = new TopDocCollector(hitsPerPage);
	    searcher.search(query, collector);
	    hits = collector.topDocs().scoreDocs;
	}
	/**
	 * Realiza una consulta de todos los resultados.
	 * @throws IOException si se produce un error de entrada/salida.
	 * @throws ParseException si se produce un error al analizar el texto.
	 */
	public void consultarM(String s) throws IOException, ParseException{
		// 2. generar consulta.
	    String querystr = s;

	    // el argumento "title" especifica el campo por defecto
	    // a usar cuando no hay ninguno por defecto.
	    QueryParser parser = new QueryParser("contenido", new StandardAnalyzer());  
        Query query = parser.parse(querystr);  
          
	    //Query q = new QueryParser("title", analyzer).parse(querystr);

	    // 3. busqueda
	    int hitsPerPage = 1000;
	    searcher = new IndexSearcher("c:/lucene_output_dir_index");
	    TopDocCollector collector = new TopDocCollector(hitsPerPage);
	    searcher.search(query, collector);
	    hits = collector.topDocs().scoreDocs;
	}
	/**
	 * Realiza una consulta de un resultado.
	 * @throws IOException si se produce un error de entrada/salida.
	 * @throws ParseException si se produce un error al analizar el texto.
	 */
	public void consultarU(String s) throws IOException, ParseException{
		// 2. generar consulta.
	    String querystr = s;

	    // el argumento "title" especifica el campo por defecto
	    // a usar cuando no hay ninguno por defecto.
	    QueryParser parser = new QueryParser("contenido", new StandardAnalyzer());  
        Query query = parser.parse(querystr);  
          
	    //Query q = new QueryParser("title", analyzer).parse(querystr);

	    // 3. busqueda
	    int hitsPerPage = 1;
	    searcher = new IndexSearcher("c:/lucene_output_dir_index");
	    TopDocCollector collector = new TopDocCollector(hitsPerPage);
	    searcher.search(query, collector);
	    hits = collector.topDocs().scoreDocs;
	}
	
	/**
	 * Método nos entrega los resultados en una cadena de salida.
	 * @throws IOException si se produce alg�n error de entrada/salida.
	 */
	public String mostrarResultados() throws IOException{
		//4.- mostrar resultados
	    String s = "Se han encontrado " + hits.length + " casos.\n";
	    for(int i=0;i<hits.length;++i) {
	      int docId = hits[i].doc;
	      Document d = searcher.doc(docId);
	      s = s + (i + 1) + ". " + d.get("titulo") + "\n";
	    }

	    // hemos acabado el proceso, asi que cerramos el quiosco. 
	    searcher.close();
	    
	    return s;
	}
	
	/**
	 * Busca con Lucene.
	 * @param entrada el directorio de entrada a indexar.
	 * @param consulta la consulta a realizar.
	 * @return donde están esos resultados.
	 */
	public String buscarLucene(String entrada, String consulta){
		String s = "Algo fue mal";
		try {
			indexar(entrada);
		} catch (IOException e) {
			System.err.println("Ups, algo ha ido mal al indexar.");
			e.printStackTrace();
		} catch (OpenXML4JException e) {
			System.err.println("Ups, algo ha ido mal al indexar un pptx.");
			e.printStackTrace();
		}
		try {
			consultar(consulta);
		} catch (IOException e) {
			System.err.println("Ups, algo ha ido mal al generar la consulta.");
			e.printStackTrace();
		} catch (ParseException e) {
			System.err.println("Ups, algo ha ido mal al analizar.");
			e.printStackTrace();
		}
		try {
			s = mostrarResultados();
		} catch (IOException e) {
			System.err.println("Ups, algo ha ido mal al intentar mostrar el resultado.");
			e.printStackTrace();
		}
		return s;
	}
	
		
	/**
	 * M�todo de prueba de la clase.
	 * @param entrada la ruta de directorio entrada.
	*/
	public void prueba(String entrada){
		System.out.println("Buscando");
		Buscador busq= new Buscador();
		try {
			busq.indexar(entrada);
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (OpenXML4JException e) {
			System.err.println("Error al indexar un pptx");
			e.printStackTrace();
		}
		String palabra = "man";
		try {
			busq.consultar(palabra);
		} catch (IOException e) {
			
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
}
