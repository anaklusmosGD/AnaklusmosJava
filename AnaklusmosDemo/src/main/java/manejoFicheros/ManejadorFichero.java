package manejoFicheros;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Clase para el manejo rápido de un fichero.
 * @author Mª Angeles Broullón Lozano.
 * @version 1.0
 *
 */
public class ManejadorFichero {
	
	/**
	 * Método que lee el contenido de un fichero línea a línea, guardando cada una de ella en un nodo de una lista.
	 * @param ruta la ruta del archivo a leer.
	 * @return el contenido de un fichero por líneas.
	 */
	public static List<String> leer(String ruta){
		List<String> contenido = new LinkedList<String>();
		File archivo = new File(ruta);
		FileReader fr = null;
		BufferedReader br = null;
		String leido = "";
			
		// Lectura del fichero		
		try {
			fr = new FileReader (archivo);
			br = new BufferedReader(fr);
			leido = br.readLine();
			while (leido != null){
				contenido.add(leido);
				leido = br.readLine();
			}			
		} catch (FileNotFoundException e) {
			System.err.println("Se ha producido un error en el acceso al fichero");
			System.err.println(e.getStackTrace());
		} catch (Exception e) {
			System.err.println("Se ha producido un error en la operación de lectura");
			System.err.println(e.getStackTrace());
		}finally{
	         // cerramos el fichero
	         try{                    
	            if(fr != null ){   
	               fr.close();     
	            }                  
	         }catch (Exception e){ 
	        	 System.err.println("Se ha producido un error al cerrar el fichero.");
	         }
			
		}
		return contenido;
	}
	
	/**
	 * Método que lee el contenido de un fichero, guardandolo todo en una cadena.
	 * @param ruta la ruta del archivo a leer.
	 * @return el contenido de un fichero, todo en una cadena de texto.
	 */
	public static String leerTodo(String ruta){
		String contenido = "";
		List<String> listaContenido = leer(ruta);
		for (int i=0; i<listaContenido.size(); i++){
			contenido = contenido + listaContenido.get(i) + "\n";
		}
		return contenido;
	}
	
	/**
	 * Método que escribe el contenido de una lista de cadenas en un fichero, a continuación de lo existente.
	 * @param ruta la ruta del archivo en el que queremos escribir.
	 * @param entrada lista con que contiene en cada nodo las líneas a escribir.
	 */
	public static void escribir(String ruta, List<String> entrada){
		//manejador de fichero y puntero para escribir
		FileWriter fichero = null;
		PrintWriter pw = null;
		Iterator<String> it = entrada.iterator();
		try{
			//abre añadiendos la líneas nuevas al cotenido previo. 
			fichero = new FileWriter(ruta, true);
			pw = new PrintWriter(fichero, true);
			while(it.hasNext()){
				pw.println(it.next());
			}			
		} catch (FileNotFoundException e) {
			System.err.println("Se ha producido un error en el acceso al fichero");
			System.err.println(e.getStackTrace());
		} catch (IOException e) {
			System.err.println("Se ha producido un error en la operación de lectura");
			System.err.println(e.getStackTrace());
        } finally {
           // nos asegurarnos de que se cierra el fichero.
	       try{
	    	   if(null != fichero){
	    		   fichero.close();
	    	   }
	       }
	       catch (Exception e){
	    	   System.err.println("Se ha producido un error");
	    	   }
        }
	}
	
	/**
	 * Método que escribe el contenido de una lista de cadenas en un fichero, eliminado todo el contenido previo del fichero.
	 * @param ruta la ruta del archivo en el que queremos escribir.
	 * @param entrada lista con que contiene en cada nodo las l�neas a escribir.
	 */
	public static void sobreescribir(String ruta, List<String> entrada){
		//manejador de fichero y puntero para escribir
		FileWriter fichero = null;
		PrintWriter pw = null;
		Iterator<String> it = entrada.iterator();
		try{
			//abre a�adiendos la l�neas nuevas al cotenido previo. 
			fichero = new FileWriter(ruta);
			pw = new PrintWriter(fichero, true);
			while(it.hasNext()){
				pw.println((String) it.next());
			}			
		} catch (FileNotFoundException e) {
			System.err.println("Se ha producido un error en el acceso al fichero");
			System.err.println(e.getStackTrace());
		} catch (IOException e) {
			System.err.println("Se ha producido un error en la operación de lectura");
			System.err.println(e.getStackTrace());
        } finally {
           // nos asegurarnos de que se cierra el fichero.
	       try{
	    	   if(null != fichero){
	    		   fichero.close();
	    	   }
	       }
	       catch (Exception e){
	    	   System.err.println("Se ha producido un error");
	    	   }
        }
	}
	
	/**
	 * Prueba del funcionamiento del manejador
	 */
	public static void prueba(){
		//escritura simple, desde 0, y posterior lectura
		List<String> l = new LinkedList<String>();
		List<String> m = new LinkedList<String>();
		l.add("hola");
		l.add("mundo");
		sobreescribir("pruebaEscritura.txt", l);
		m = leer("pruebaEscritura.txt");
		System.out.println(m.toString());
		//escritura simple, desde 0, y posterior lectura
		l = new LinkedList<String>();
		l.add("adios");
		l.add("mundo");
		sobreescribir("pruebaEscritura.txt", l);
		m = leer("pruebaEscritura.txt");
		System.out.println(m.toString());
		//escritura a continuaci�n, desde 0, y posterior lectura
		l.add("hola");
		l.add("mundo");
		l.add("otra vez");
		escribir("pruebaEscritura.txt", l);
		m = leer("pruebaEscritura.txt");
		System.out.println(m.toString());
	}
}


