package manejoFicheros;

/**
 * Clase que controla el tipo de fichero que estaremos tratando.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 0.1
 */
public class ClasificadorDeFicheros {

	// Métodos

	/**
	 * Método observador que nos permite saber si el fichero es de tipo pdf.
	 * 
	 * @param nombreFichero
	 *            el nombre del fichero a comprobar.
	 * @return true si es txt, false en caso contrario.
	 */
	public static boolean esPdf(String nombreFichero) {
		return nombreFichero.endsWith(".pdf");
	}

	/**
	 * Método observador que nos permite saber si el fichero es de tipo doc
	 * (Word 97-2003).
	 * 
	 * @param nombreFichero
	 *            el nombre del fichero a comprobar.
	 * @return true si es doc, false en caso contrario.
	 */
	public static boolean esDoc(String nombreFichero) {
		return nombreFichero.endsWith(".doc");
	}

	/**
	 * Método observador que nos permite saber si el fichero es de tipo txt.
	 * 
	 * @param nombreFichero
	 *            el nombre del fichero a comprobar.
	 * @return true si es pdf, false en caso contrario.
	 */
	public static boolean esTxt(String nombreFichero) {
		return nombreFichero.endsWith(".txt");
	}

	/**
	 * Método observador que nos permite saber si el fichero es de tipo xls
	 * (Excel 97-2003).
	 * 
	 * @param nombreFichero
	 *            el nombre del fichero a comprobar.
	 * @return true si es xls, false en caso contrario.
	 */
	public static boolean esXls(String nombreFichero) {
		return nombreFichero.endsWith(".xls");
	}

	/**
	 * Método observador que nos permite saber si el fichero es de tipo ppt
	 * (Powerpoint 97-2003).
	 * 
	 * @param nombreFichero
	 *            el nombre del fichero a comprobar.
	 * @return true si es ppt, false en caso contrario.
	 */
	public static boolean esPpt(String nombreFichero) {
		return nombreFichero.endsWith(".ppt");
	}

	/**
	 * Método observador que nos permite saber si el fichero es de tipo vsd
	 * (Visio 97-2003).
	 * 
	 * @param nombreFichero
	 *            el nombre del fichero a comprobar.
	 * @return true si es vsd, false en caso contrario.
	 */
	public static boolean esVsd(String nombreFichero) {
		return nombreFichero.endsWith(".vsd");
	}
	
	/**
	 * Método observador que nos permite saber si el fichero es de tipo pub
	 * (Publisher 97-2003).
	 * 
	 * @param nombreFichero
	 *            el nombre del fichero a comprobar.
	 * @return true si es pub, false en caso contrario.
	 */
	public static boolean esPub(String nombreFichero) {
		return nombreFichero.endsWith(".pub");
	}

	/**
	 * Método observador que nos permite saber si el fichero es de tipo msg
	 * (Outlook 98-2007).
	 * 
	 * @param nombreFichero
	 *            el nombre del fichero a comprobar.
	 * @return true si es msg, false en caso contrario.
	 */
	public static boolean esMsg(String nombreFichero) {
		return nombreFichero.endsWith(".msg");
	}

	/**
	 * Método observador que nos permite saber si el fichero es de tipo xlsx
	 * (nuevo Excel).
	 * 
	 * @param nombreFichero
	 *            el nombre del fichero a comprobar.
	 * @return true si es xlsx, false en caso contrario.
	 */
	public static boolean esXlsx(String nombreFichero) {
		return nombreFichero.endsWith(".xlsx");
	}

	/**
	 * Método observador que nos permite saber si el fichero es de tipo docx
	 * (nuevo Word).
	 * 
	 * @param nombreFichero
	 *            el nombre del fichero a comprobar.
	 * @return true si es docx, false en caso contrario.
	 */
	public static boolean esDocx(String nombreFichero) {
		return nombreFichero.endsWith(".docx");
	}

	/**
	 * Método observador que nos permite saber si el fichero es de tipo pptx
	 * (nuevo Powerpoint).
	 * 
	 * @param nombreFichero
	 *            el nombre del fichero a comprobar.
	 * @return true si es pptx, false en caso contrario.
	 */
	public static boolean esPptx(String nombreFichero) {
		return nombreFichero.endsWith(".pptx");
	}

	public static boolean esFormatoValido(String nombreFichero) {
		boolean valido = false;
		valido = valido || esPdf(nombreFichero) || esDoc(nombreFichero)
				|| esTxt(nombreFichero) || esXls(nombreFichero)
				|| esPpt(nombreFichero) || esVsd(nombreFichero)
				|| esMsg(nombreFichero)	|| esXlsx(nombreFichero)
				|| esDocx(nombreFichero) || esPpt(nombreFichero)
				|| esVsd(nombreFichero) || esPub(nombreFichero);
		return valido;
	}
}
