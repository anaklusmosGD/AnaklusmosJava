package manejoFicheros;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * Clase que obtiene la lista de ficheros contenidos en una ruta concreta.
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 0.1
 */
public class Spider {

	// Métodos

	/**
	 * Método obtiene el los paths del contenido de una ruta.
	 * 
	 * @param ruta
	 *            la ruta en la que buscaremos.
	 * @return la lista de las rutas de los ficheros.
	 */
	public List<String> listar(String ruta) {
		System.out.println("¿Qué hay en " + ruta + "?");
		File f = new File(ruta);

		List<String> l = new LinkedList<String>();
		if (f.exists()) {
			File[] ficheros = f.listFiles();
			for (int i = 0; i < ficheros.length; i++) {
				l.add(ficheros[i].getAbsolutePath());
			}
		} else {
			System.out.println("Ruta incorrecta");
		}
		System.out.println("GO");
		return l;
	}

	/**
	 * Método obtiene el los paths del contenido de una ruta.
	 * 
	 * @param ruta
	 *            la ruta en la que buscaremos.
	 * @return la lista de las rutas de los ficheros.
	 */
	public List<String> listarRecursivo(String ruta) {
		File f = new File(ruta);
		List<String> l = new LinkedList<String>();
		if (f.exists()) {
			File[] ficheros = f.listFiles();
			for (int i = 0; i < ficheros.length; i++) {
				if (ficheros[i].isDirectory()) {
					List<String> l1 = listarRecursivo(ficheros[i]
							.getAbsolutePath());
					l.addAll(l1);
				} else {
					//solo añade a la lista lo parseable
					if (ClasificadorDeFicheros.esFormatoValido(ficheros[i]
							.getAbsolutePath())) {
						l.add(ficheros[i].getAbsolutePath());
					}
				}
			}
		} else {
			System.out.println("Ruta incorrecta");
		}
		return l;
	}

	/**
	 * Método de prueba de la clase.
	 */
	public void test() {
		// prueba: listado del contenido de la carpeta que realmente nos ocupa.
		String donde = "/home/thalion/Desarrollo";
		List<String> lista = listarRecursivo(donde);
		System.out.println("SALIDA");
		for (int i = 0; i < lista.size(); i++) {
			System.out.println(lista.get(i));
		}
	}

	public static void main(String[] args) {
		Spider s = new Spider();
		s.test();
	}

}
