package manejoFicheros;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.poi.hdgf.extractor.VisioTextExtractor;
import org.apache.poi.hpbf.extractor.PublisherTextExtractor;
import org.apache.poi.hslf.extractor.PowerPointExtractor;
import org.apache.poi.hsmf.MAPIMessage;
import org.apache.poi.hsmf.exceptions.ChunkNotFoundException;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFPictureData;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

/**
 * Clase que controla la lectura de ficheros de microsoft. Usa Apache POI:
 * http://poi.apache.org/
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 0.2
 */
public class LectorFormatos {

	/**
	 * Lee un archivo de Word.
	 * 
	 * @param nombreFichero
	 *            nombre del fichero a recorrer.
	 * @return el contenido del fichero Word en una cadena.
	 * @throws IOException
	 *             si no se puede acceder al fichero.
	 */
	public static String leerWord(String nombreFichero) throws IOException {

		String datos = "";

		FileInputStream fis = null;
		try {
			fis = new FileInputStream(nombreFichero);
			HWPFDocument word = new HWPFDocument(fis);
			datos = datos + word.getDocumentText();
			fis.close();
		} catch (FileNotFoundException e) {
			System.err
					.println("No se pudo acceder al fichero " + nombreFichero);
			throw new FileNotFoundException("No se pudo acceder al fichero "
					+ nombreFichero);
		} catch (IOException e) {
			System.err.println("Se produjo un error de excel al trabajar con "
					+ nombreFichero);
			throw new IOException(
					"Se produjo un error de excel al trabajar con "
							+ nombreFichero);
		}
		return datos;
	}

	/**
	 * Lee un archivo de Excel.
	 * 
	 * @param nombreFichero
	 *            nombre del fichero a recorrer.
	 * @return el contenido del fichero Excel en una cadena.
	 * @throws IOException
	 *             si no se puede acceder al fichero.
	 */
	public static String leerExcel(String nombreFichero) throws IOException {

		String datos = "";
		FileInputStream fis = null;

		try {
			fis = new FileInputStream(nombreFichero);

			// crear un libro de excel.
			HSSFWorkbook workbook = new HSSFWorkbook(fis);
			// recorre las hojas del libro
			int numHojas = workbook.getNumberOfSheets();
			for (int i = 0; i < numHojas; i++) {
				HSSFSheet hoja = workbook.getSheetAt(i);
				// recorre las filas de la hoja
				for (int j = hoja.getFirstRowNum(); j < hoja.getLastRowNum(); j++) {
					HSSFRow fila = hoja.getRow(j);
					// recorre las columnas de la hoja
					for (int k = fila.getFirstCellNum(); k < fila
							.getLastCellNum(); k++) {
						datos = datos + fila.getCell(k).toString();
						if (k < fila.getLastCellNum() - 1) {
							datos = datos + ", ";
						}
					}
					datos = datos + "\n";
				}
			}
			fis.close();
		} catch (FileNotFoundException e) {
			System.err
					.println("No se pudo acceder al fichero " + nombreFichero);
			throw new FileNotFoundException("No se pudo acceder al fichero "
					+ nombreFichero);
		} catch (IOException e) {
			System.err.println("Se produjo un error de excel al trabajar con "
					+ nombreFichero);
			throw new IOException(
					"Se produjo un error de excel al trabajar con "
							+ nombreFichero);
		}
		return datos;
	}

	/**
	 * Lee un archivo de PowerPoint.
	 * 
	 * @param nombreFichero
	 *            nombre del fichero a recorrer.
	 * @return el contenido del fichero PowerPoint en una cadena.
	 * @throws IOException
	 *             si no se puede acceder al fichero.
	 */
	public static String leerPowerPoint(String nombreFichero) throws IOException {
		String datos = "";
		InputStream is = new FileInputStream(nombreFichero);
		PowerPointExtractor extractor = new PowerPointExtractor(is);
		// obtiene el texto normal y el de las anotaciones.
		datos = extractor.getText(true, true);
		is.close();
		return datos;
	}

	/**
	 * Lee un archivo de Outlook.
	 * 
	 * @throws IOException
	 *             si no se puede acceder al fichero.
	 */
	public static String leerOutlook(String nombreFichero) throws IOException {
		// la salida
		String datos = "";
		// el mensaje que procesaremos.
		MAPIMessage msg = new MAPIMessage(nombreFichero);
		try {
			String displayFrom = msg.getDisplayFrom();
			datos = datos + ("From: " + displayFrom) + "\n";
		} catch (ChunkNotFoundException e) {
			// ignorarlo
		}
		try {
			String displayTo = msg.getDisplayTo();
			datos = datos + ("To: " + displayTo) + "\n";
		} catch (ChunkNotFoundException e) {
			// ignorarlo
		}
		try {
			String displayCC = msg.getDisplayCC();
			datos = datos + ("CC: " + displayCC) + "\n";
		} catch (ChunkNotFoundException e) {
			// ignorarlo
		}
		try {
			String displayBCC = msg.getDisplayBCC();
			datos = datos + ("BCC: " + displayBCC) + "\n";
		} catch (ChunkNotFoundException e) {
			// ignorarlo
		}
		try {
			String subject = msg.getSubject();
			datos = datos + ("Subject: " + subject) + "\n";
		} catch (ChunkNotFoundException e) {
			// ignore
		}
		try {
			String body = msg.getTextBody();
			datos = datos + (body) + "\n";
		} catch (ChunkNotFoundException e) {
			System.err.println("No hay cuerpo de mensaje");
			throw new IOException("No hay cuerpo de mensaje en "
					+ nombreFichero);
		}
		return datos;

	}

	/**
	 * Lee un archivo de Publisher.
	 * 
	 * @param nombreFichero
	 *            nombre del fichero a recorrer.
	 * @return el contenido del fichero Publisher en una cadena.
	 * @throws IOException
	 *             si no se puede acceder al fichero.
	 */
	public static String leerPublisher(String nombreFichero) throws IOException {
		String datos = "";
		InputStream is = new FileInputStream(nombreFichero);
		PublisherTextExtractor extractor = new PublisherTextExtractor(is);
		datos = extractor.getText();
		is.close();
		return datos;
	}

	/**
	 * Lee un archivo de Visio.
	 * 
	 * @param nombreFichero
	 *            nombre del fichero a recorrer.
	 * @return el contenido del fichero Visio en una cadena.
	 * @throws IOException
	 *             si no se puede acceder al fichero.
	 */
	public static String leerVisio(String nombreFichero) throws IOException {
		String datos = "";
		InputStream is = new FileInputStream(nombreFichero);
		VisioTextExtractor extractor = new VisioTextExtractor(is);
		datos = extractor.getText();
		is.close();
		return datos;
	}
	
	/**
	 * Lee un archivo de Word extendido (docX).
	 * 
	 * @param nombreFichero
	 *            nombre del fichero a recorrer.
	 * @return el contenido del fichero Word extendido en una cadena.
	 * @throws IOException
	 *             si no se puede acceder al fichero.
	 */
	public static String leerWordX(String nombreFichero) throws IOException {

		String datos = "";

		FileInputStream fis = null;
		try {
			fis = new FileInputStream(nombreFichero);
			
			XWPFDocument word = new XWPFDocument(fis);
			List<XWPFParagraph> parrafos = word.getParagraphs();
			for (int i=0; i<parrafos.size(); i++){
				datos = datos + (parrafos.get(i)).getText();
			}
			fis.close();
		} catch (FileNotFoundException e) {
			System.err
					.println("No se pudo acceder al fichero " + nombreFichero);
			throw new FileNotFoundException("No se pudo acceder al fichero "
					+ nombreFichero);
		} catch (IOException e) {
			System.err.println("Se produjo un error de excel al trabajar con "
					+ nombreFichero);
			throw new IOException(
					"Se produjo un error de excel al trabajar con "
							+ nombreFichero);
		}
		return datos;
	}
	
	/**
	 * Lee un archivo de Excel extendido (xlsx).
	 * 
	 * @param nombreFichero
	 *            nombre del fichero a recorrer.
	 * @return el contenido del fichero Excel en una cadena.
	 * @throws IOException
	 *             si no se puede acceder al fichero.
	 */
	public static String leerExcelX(String nombreFichero) throws IOException {

		String datos = "";
		FileInputStream fis = null;

		try {
			fis = new FileInputStream(nombreFichero);

			// crear un libro de excel.
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			// recorre las hojas del libro
			int numHojas = workbook.getNumberOfSheets();
			for (int i = 0; i < numHojas; i++) {
				XSSFSheet hoja = workbook.getSheetAt(i);
				// recorre las filas de la hoja
				for (int j = hoja.getFirstRowNum(); j < hoja.getLastRowNum(); j++) {
					XSSFRow fila = hoja.getRow(j);
					// recorre las columnas de la hoja
					for (int k = fila.getFirstCellNum(); k < fila
							.getLastCellNum(); k++) {
						datos = datos + fila.getCell(k).toString();
						if (k < fila.getLastCellNum() - 1) {
							datos = datos + ", ";
						}
					}
					datos = datos + "\n";
				}
			}
			fis.close();
		} catch (FileNotFoundException e) {
			System.err
					.println("No se pudo acceder al fichero " + nombreFichero);
			throw new FileNotFoundException("No se pudo acceder al fichero "
					+ nombreFichero);
		} catch (IOException e) {
			System.err.println("Se produjo un error de excel al trabajar con "
					+ nombreFichero);
			throw new IOException(
					"Se produjo un error de excel al trabajar con "
							+ nombreFichero);
		}
		return datos;
	}

	/**
	 * Lee un archivo de PowerPoint extendido (pptx).
	 * 
	 * @param nombreFichero
	 *            nombre del fichero a recorrer.
	 * @return el contenido del fichero PowerPoint extendido en una cadena.
	 * @throws IOException
	 *             si no se puede acceder al fichero.
	 * @throws OpenXML4JException si no hay una lectura correcta del contenido.
	 */
	public static String leerPowerPointX(String nombreFichero) throws IOException, OpenXML4JException {
		String datos = "";
		InputStream is = new FileInputStream(nombreFichero);
		XMLSlideShow ppt = new XMLSlideShow(is);

		// Obtiene los objetos embebidos.
		List<PackagePart> embebidos = ppt.getAllEmbedds();
		for (PackagePart p : embebidos) {
			datos = datos + "tipo: " + p.getContentType() + "\n";
			datos = datos + "nombre: " + p.getPartName().getName() + "\n";
			// en general es un nombre de archivo			

			InputStream pIs = p.getInputStream();
			// los datos
			datos = datos + pIs.toString() + "\n";
			pIs.close();
		}
		// Obtiene los documentos embebidos.
		List<XSLFPictureData> imagenes = ppt.getAllPictures();
		for (XSLFPictureData data : imagenes) {
			PackagePart p = data.getPackagePart();

			datos = datos + "tipo: " + p.getContentType() + "\n";
			datos = datos + "nombre: " + data.getFileName() + "\n";

			InputStream pIs = p.getInputStream();
			// obtine los datos de imagenes
			datos = datos + pIs.toString() + "\n";
			pIs.close();

		}
		
		is.close();
		return datos;
	}

}
