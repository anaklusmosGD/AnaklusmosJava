package manejoFicheros;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

/**
 * Clase que controla la lectura de ficheros Pdf.
 * Usa PDFBox: http://pdfbox.apache.org/
 * 
 * @author Mª Angeles Broullón Lozano.
 * @version 1.0
 */
public class LectorPdf {

	/**
	 * Extrae el texto del pdf y lo pone en una cadena.
	 * 
	 * @param ruta
	 *            la ruta del fichero cuyo texto queremos obtener.
	 * @return una cadena de texto con el contenido del pdf.
	 * @throws IOException
	 *             en caso de que no se logre abrir el fichero o parsearlo
	 *             correctamente.
	 */
	public static String extraerTextoDePdf(String ruta) throws IOException {
		PDFParser parser;
		System.out.println("Parseo un pdf");
		String parsedText = "";

		File f = new File(ruta);
		if (!f.isFile()) {
			System.err.println("El fichero " + ruta + " no existe.");
			return null;
		}

		parser = new PDFParser(new FileInputStream(f));
		parser.parse();
		COSDocument cosDoc = parser.getDocument();
		PDFTextStripper pdfStripper = new PDFTextStripper();
		PDDocument pdDoc = new PDDocument(cosDoc);
		parsedText = pdfStripper.getText(pdDoc);

		return parsedText;
	}
	
	/**
	 * M�todo de prueba.
	 */
	public void test(){
		String ruta = "/home/thalion/Desarrollo/1.pdf";
		String texto = "se intentó sin éxito";
		try {
			texto = extraerTextoDePdf(ruta);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(texto);		
	}	

}
