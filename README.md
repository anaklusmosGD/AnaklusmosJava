# Anaklusmos

## Origins

These document management project was born as many free softwares do, while talking and drinking coffe. Around March 2012, Mª Angeles Broullón and Mariano Cascon were discussing the needs of small business for new solutions to manage their data during the economic crisis.

Due to the lack of resources, many business had to move to a smaller office but they had problems to store so much paperwork. Digitalize that information was the obvious version, but that would mean to work with a digital archive which followed the Spanish laws to protect their data, so they would need new (and expensive) software.

Then, we started to discuss which technology would allow us to generate a set of alternative applications, platform and operating-system independent:
* A system to index documents.
* A system to search contents.
* A system to manage groups for access.
* Securizing the system, following the Spanish laws (Oficial de Protección de datos).
* Take the platform to the net, so it's available anywhere.

Mª Angeles (back-end developer) developed a first fast prototype in a week on Java to index txt and pdf files. Later, Juan José Arol (front-end developer) and Ana Mª Fernandez (server management) joined the project.

On July 2013 we were asked to generate a [Prezi](VideoDemo/Proyecto Anaklusmos by Angeles Broullon Lozano on Prezi.mp4) presentation.


## Anaklusmos in action

Version 0.1 - Java desktop prototype
* Java prototype
![Java prototype](VideoDemo/Anaklusmos Demo del Prototipo 0.mp4)

Version 0.5 - Web prototype
* Administrator mode:
![Administrator mode](VideoDemo/Anaklusmos demo del prototipo web 0.5 - administración de grupos.mp4)
* Supervisor mode:
![Supervisor mode](VideoDemo/Anaklusmos demo del prototipo web 0.5 - modo supervisor.mp4)
* Guest mode:
![Guest mode](VideoDemo/Anaklusmos demo del prototipo web 0.5 - modos invitado y usuario.mp4)


## This project was developed using
* Ubuntu
* Eclipse IDE
* Apache Maven
* Spring Framework
* Brackets
* Dia (diagram editor)
* Kazam (screen recorder)
* Openshot (video editor)

## Who worked on this project
 *  Mª Angeles Broullón - Back-end developer
 * Juan José Arol - Front-end developer
 * Mariano Cascón - Commercial insight
 * Ana Mª Fernández - Server management

More information about the development process can be found on the wiki (in Spanish)

AnaklusmosGD is licensed under a International [Creative Commons By License](https://creativecommons.org/licenses/by/4.0/)
